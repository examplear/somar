<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Monolog\Logger;

class se_Sirjames extends BaseScraper {

  const Company_Id = 9071592;

  protected CSVReader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new CSVReader(parent::GetUrl(),"se_Sirjames",",");
  }
  public function GetRandomIndex(): int {
      return rand(1, 100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {

    // loop through the file line-by-line
    if(($data = $this->reader->Next()) === NULL){
      return NULL;
    }

    $offer = $data;
    if($offer && isset($offer[0])){

      $node = [];
      $node["format"] = "full_html";
      $node["company_id"] = self::Company_Id; //Constant from Drupal;
      $node["product_id"] = 'sirjames-se-'.substr(md5((string)$offer[14]),0,10);
      $node["description"] = htmlspecialchars((string)$offer[3]);
      $node["title"] = htmlspecialchars((string)$offer[9]);
      $node["raw_brand"] = utf8_decode((string)$offer[7]);
      $node["old_price"] = SH::extractPrice((string)$offer[10],",");     
      $node["image_small_url"] = (string)$offer[5];
      $node["url"] = utf8_decode((string)$offer[14]);
   
  
    $node["scrape"] = ["html" => ["url" => urldecode(substr($node["url"],strpos($node["url"],'redirect=')+9)), "method" => "GET"]];

     if(isset($offer[0]) && !empty($offer[0])){
        $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=de&name_hash='.md5(mb_strtolower(trim(htmlspecialchars((string)$offer[0])),'UTF-8'));

       $c = json_decode(file_get_contents($catpath));
    
       $node["raw_categories"] = htmlspecialchars((string)$offer[0]);

            if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
             $ids =  explode(',', $c[0]->tax_group);
             foreach($ids as $id){
               $node["categories"][] = (int) $id;
             }
        }

      }
      return $node;
    }
    return NULL;
  }

  public function ExternalData(array &$node, array $scraped){


    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"].":".$node["scrape"]["html"]["url"]." ";
      return;
    }
    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);
    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;

      $node["new_price"] = SH::extractPrice(HH::qVal($crawler, ".product-shop-info .price-box .special-price .price"), "", ".");

      unset($crawler);
    }

    return $node;

  }
}





























 
