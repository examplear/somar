<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;

class se_Verktygshandlarn extends BaseScraper {

  //const MQ_URL = "/vagrant/www/mq_demo_2.xml";
  const Company_Id = 7434141;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Verktygshandlarn");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id;
    $node["product_id"] = 'verktygshandlarn-'.(string)$offer->SKU;
    $node["url"] = (string)$offer->productUrl;
    $node["title"] = utf8_decode((string)$offer->name);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->regularPrice,",");
    $node["image_small_url"] = (string)$offer->graphicUrl;
    $node["description"] = utf8_decode((string)$offer->description);
    $node["raw_categories"] = utf8_decode(parent::Combine($offer->categories->category));

    return $node;
  }


  public function ExternalData(array &$node, array $html){

  }


}
