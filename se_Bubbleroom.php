<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class se_Bubbleroom extends BaseScraper {

  const Company_Id = 8542689;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Bubbleroom");
  }
  public function GetRandomIndex(): int {
      return rand(30000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'bubbleroom-'.(string)$offer->SKU.'-'.substr(md5((string)$offer->ProductUrl),0,5);
    $node["url"] = utf8_decode((string)$offer->TrackingUrl);
    $node["title"] = utf8_decode(trim((string)$offer->Name));
    $node["raw_brand"] = utf8_decode((string)$offer->Brand);
    $node["new_price"] = SH::extractPrice((string)$offer->Price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->OriginalPrice,",");
    $node["image_small_url"] = (string)$offer->ImageUrl;
    $node["description"] = utf8_decode((string)$offer->Description);
    $node["raw_categories"] = utf8_decode((string)$offer->Category);


    $nam =  (array) $offer->Extras->Name;
    $val =  (array) $offer->Extras->Value;

    if($nam[0] == 'GENDER'){
        $gen = $val[0];
    }
    else if($nam[1] == 'GENDER'){
        $gen = $val[1];
    }
    else if($nam[2] == 'GENDER'){
        $gen = $val[2];
    }
    else if($nam[3] == 'GENDER'){
        $gen = $val[3];
    }
    else{
      $gen = '';
    }

    if($gen == "female"){
      $node["gender"] = 1;

    } elseif($gen == "male"){
      $node["gender"] = 0;
    }
    else {
      $node["gender"] = 2;
    }


    $nam1 =  (array) $offer->Extras->Name;
    $val1 =  (array) $offer->Extras->Value;

    if($nam1[0] == 'COLOUR'){
        $col1 = $val1[0];
    }
    else if($nam1[1] == 'COLOUR'){
        $col1 = $val1[1];
    }
    else if($nam1[2] == 'COLOUR'){
        $col1 = $val1[2];
    }
    else if($nam1[3] == 'COLOUR'){
        $col1 = $val1[3];
    }
    else{
      $col1 = '';
    }

    $node["raw_colors"] = utf8_decode($col1);



    $nam2 =  (array) $offer->Extras->Name;
    $val2 =  (array) $offer->Extras->Value;

    if($nam2[0] == 'EXTRA_INFO'){
        $size2 = $val2[0];
    }
    else if($nam2[1] == 'EXTRA_INFO'){
        $size2 = $val2[1];
    }
    else if($nam2[2] == 'EXTRA_INFO'){
        $size2 = $val2[2];
    }
    else if($nam2[3] == 'EXTRA_INFO'){
        $size2 = $val2[3];
    }
    else{
      $size2 = '';
    }

    $node["raw_sizes"] = array($size2);



 return $node;
  }

  public function ExternalData(array &$node, array $html){

  }
}
