<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class se_Footway extends BaseScraper {

  const Company_Id = 1220108;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Footway");
  }
  public function GetRandomIndex(): int {
      return rand(30000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'footway-se-'.(string)$offer->sku.'-'.substr(md5((string)$offer->productUrl),0,5);
    $node["url"] = (string)$offer->productUrl;
    $node["new_price"] = SH::extractPrice((string)$offer->priceWithTax,",");
    $node["old_price"] = SH::extractPrice((string)$offer->originalPriceWithTaxt,",");
    $node["title"] = utf8_decode(trim((string)$offer->title));
    $node["image_small_url"] = (string)$offer->images->thumbnail;
    $node["raw_sizes"] = [(string)$offer->sizesInStock->size];
    $node["raw_colors"] = [(string)$offer->color];
    $node["description"] = utf8_decode((string)$offer->description);
    $node["raw_brand"] = utf8_decode((string)$offer->brand);


    if(isset($offer->categories->category) && $offer->categories->category!=''){

        $cats = $offer->categories->category;

        if(count($cats)<2){
           $maincat = str_replace('http://www.footway.se/','',$cats);
        }
        else{
          $maincat ='';

          foreach ($cats as $cat) {
            $maincat = str_replace('http://www.footway.se/','',$maincat);
            $maincat =  $maincat.str_replace('http://www.footway.se/','',$cat);
          }
        }

        $subcats = $offer->subCategories->subCategory;
        if(count($subcats)<1){
          $categor = str_replace('http://www.footway.se/','',$maincat);
        }
        else{
          $categor = '';
          foreach($subcats as $subcat){
            $maincat = str_replace('http://www.footway.se/','',$maincat);
            $categor .=  $maincat.str_replace('http://www.footway.se/','',$subcat);
          }
        }

        if(preg_match("/^(?=.*herrskor)(?=.*damskor)/i", $categor)){
            $node["gender"] = 2;
        }
        else if(preg_match("/herrskor/", $categor)){
            $node["gender"] = 0;
        }
        else if(preg_match("/damskor/", $categor)){
            $node["gender"] = 1;
          }
        else
        {
           $node["gender"] = null;
        }

        $catpath = 'http://beta.smartster.no/driver.php?table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(utf8_decode($categor)),'UTF-8'));

        $c = json_decode(file_get_contents($catpath));

        $node["raw_categories"] = utf8_decode($categor);
          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
             foreach($ids as $id){
               $node["categories"][] = (int) $id;
             }
        }
    }

    return $node;
  }


  public function ExternalData(array &$node, array $html){

  }

}
