<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Monolog\Logger;

class se_Itab_Outdoor extends BaseScraper {
  const Company_Id = 6469384;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_itab_outdoor");
  }

  public function GetRandomIndex(): int {
      return rand(1000, 100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }

  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next();
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;

    $node["product_id"] = 'itab_outdoor-'.(string)$offer->SKU;
    $node["url"] = (string)$offer->productUrl;
    $node["title"] = htmlspecialchars_decode((string)$offer->name);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->regularPrice,",");
    $node["image_small_url"] = (string)$offer->graphicUrl;

    $node["description"] = htmlspecialchars_decode((string)$offer->description);
    $node["summary"] = htmlspecialchars_decode(SH::text_summary((string)$offer->description));
    $node["raw_brand"] = htmlspecialchars_decode((string)$offer->brand);


    $node["raw_categories"] = htmlspecialchars_decode(parent::Combine($offer->categories->category));


    unset($offer);
    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }
}
