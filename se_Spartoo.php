<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;

class se_Spartoo extends BaseScraper {

  const Company_Id = 35;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Spartoo");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'spartoo-se-'.(string)$offer->TDProductId.'-'.substr(md5((string)$offer->productUrl),0,5);
    $node["url"] = (string)$offer->productUrl;
    $node["new_price"] = (string)$offer->price;
    $node["old_price"] = (string)$offer->previousPrice;
    $node["title"] = utf8_encode(preg_replace('/\s\s+/', ' ', (string)$offer->name));
    $node["image_small_url"] = (string)$offer->fields->imageLarge;
    //$node["raw_sizes"] = [(string)$offer->size];
    //$node["raw_colors"] = [utf8_decode((string)$offer->color)];
    $node["description"] = utf8_encode(preg_replace('/\s\s+/', ' ', (string)$offer->description));
    $node["summary"] = utf8_encode(preg_replace('/\s\s+/', ' ', (string)$offer->description));
    $node["raw_brand"] = utf8_encode(preg_replace('/\s\s+/', ' ', (string)$offer->brand));


    if(isset($offer->merchantCategoryName) && !empty($offer->merchantCategoryName)){
        $catpath = 'http://beta.smartster.no/driver.php?table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim((string)$offer->merchantCategoryName),'UTF-8'));

        $c = json_decode(file_get_contents($catpath));

        $node["raw_categories"] = utf8_encode((string)$offer->merchantCategoryName);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
         $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }



    return $node;
  }

   public function ExternalData(array &$node, array $html){

  }


}








/* <?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Monolog\Logger;

class se_Spartoo extends BaseScraper {

  //const Company_Id = 1004;
  const Company_Id = 35;

  protected CSVReader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new CSVReader(parent::GetUrl(),"se_Spartoo","\t");
  }
  public function GetRandomIndex(): int {
      return rand(1, 100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }

  public function ScrapeNext() : ?array {

    // loop through the file line-by-line
    if(($data = $this->reader->Next()) === NULL){
      return NULL;
    }

    $offer = $data;
    // resort/rewrite data and insert into DB here
    // try to use conditions sparingly here, as those will cause slow-performance
    if($offer && isset($offer[0])){

      $node = [];


      $node["format"] = "full_html";
      //$node["country"] = "SE";
      $node["company_id"] = self::Company_Id; //Constant from Drupal;
      $node["product_id"] = 'eyerim-'.md5((string)$offer[0]);



    $node["product_id"] = 'spartoo-se-'.(string)$offer[6].'-'.substr(md5((string)$offer[1]),0,5);
      $node["title"] = ((string)$offer[0]);
      $node["description"] = trim((string)$offer[3]);
      $node["summary"] = SH::text_summary(trim((string)$offer[3]));
      $node["url"] = (string)$offer[5];
      $node["old_price"] = "500";
      $node["new_price"] = "300";
      $node["raw_brand"] = (string)$offer[12];
      $node["raw_colors"] = trim((string)$offer[16]);
      $node["raw_sizes"] = utf8_encode((string)$offer[19]);
      $node["image_small_url"] = "https://www.eyerim.no/media/catalog/product/s/r/sryb-g000025-m000073-bi-1.jpg";
      $node["image_large_url"] = "https://www.eyerim.no/media/catalog/product/s/r/sryb-g000025-m000073-bi-1.jpg";



    if(isset($offer->merchantCategoryName) && !empty($offer->merchantCategoryName)){
        $catpath = 'http://beta.smartster.no/driver.php?table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->merchantCategoryName)),'UTF-8'));

        $c = json_decode(file_get_contents($catpath));

        $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryName);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
         $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }



      unset($data);
      return $node;
    }
    return NULL;
  }
  public function ExternalData(array &$node, array $scraped){
  }
}

*/
