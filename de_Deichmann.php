<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class de_Deichmann extends BaseScraper {

  const Company_Id = 9013925;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_Deichmann");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }


    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DE";
    $node["product_id"] = 'deichmann-de-'.(string)$offer->Details->ProductID.'-'.substr(md5((string)$offer->Deeplinks->Product),0,5);
    $node["url"] = utf8_decode((string)$offer->Deeplinks->Product);
    $node["title"] = htmlspecialchars_decode(trim((string)$offer->Details->Title));
    $node["raw_brand"] = htmlspecialchars_decode((string)$offer->Details->Brand);
    $node["new_price"] = SH::extractPrice((string)$offer->Price->DisplayPrice,",");
    $node["old_price"] = SH::extractPrice((string)$offer->Price->PriceOld,",");
    $node["description"] = htmlspecialchars_decode((string)$offer->Details->Description); 
   // $node["summary"] = htmlspecialchars_decode(SH::text_summary((string)$offer->Details->Description));
    $node["image_small_url"] = (string)$offer->Images->Img->URL;

    $nam1 =  $offer->Properties->Property[0]->attributes()->Title;
    $val1 =  $offer->Properties->Property[0]->attributes()->Text;

    if($nam1[0] == 'CF_color'){
        $col1 = $val1[0];
        $node["raw_colors"] = [utf8_decode((string)$col1)];
    }
  

    $nam2 =  $offer->Properties->Property[1]->attributes()->Title;
    $val2 =  $offer->Properties->Property[1]->attributes()->Text;

    if($nam2[0] == 'CF_gender'){
        $gen = $val2[0];
   
    if($gen == "Women"){
      $node["gender"] = 1;
      $node["raw_categories"] = htmlspecialchars_decode((string)$offer->CategoryPath->ProductCategoryPath.' - Women');

    } elseif($gen == "Men"){
      $node["gender"] = 0;
       $node["raw_categories"] = htmlspecialchars_decode((string)$offer->CategoryPath->ProductCategoryPath.' - Men');
    }
    elseif($gen == "Unisex"){
       $node["gender"] = 2;
       $node["raw_categories"] = htmlspecialchars_decode((string)$offer->CategoryPath->ProductCategoryPath.' - Unisex');
    }
    else {
     $node["gender"] = NULL;
     $node["raw_categories"] = htmlspecialchars_decode((string)$offer->CategoryPath->ProductCategoryPath);
    }

}
    $nam3 =  $offer->Properties->Property[2]->attributes()->Title;
    $val3 =  $offer->Properties->Property[2]->attributes()->Text;

   if($nam3[0] == 'CF_size'){
        $size = $val3[0];
    
    $node["raw_sizes"] = explode(",",(string)$size);

}
  if(isset($offer->CategoryPath->ProductCategoryPath) && !empty($offer->CategoryPath->ProductCategoryPath)){
      $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=de&name_hash='.md5(mb_strtolower(trim($node["raw_categories"]),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

   //  $node["raw_categories"] = utf8_decode((string)$offer->CategoryPath->ProductCategoryPath);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
