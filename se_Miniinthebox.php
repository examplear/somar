<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Monolog\Logger;

class se_Miniinthebox extends BaseScraper {

  const Company_Id = 1232643;

  protected CSVReader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new CSVReader(parent::GetUrl(),"se_Miniinthebox","\t");
  }
  public function GetRandomIndex(): int {
      return rand(1, 100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {

    // loop through the file line-by-line
    if(($data = $this->reader->Next()) === NULL){
      return NULL;
    }

    $offer = $data;
    if($offer && isset($offer[6])){
      
       $node = [];
       $node["format"] = "full_html";
       $node["company_id"] = self::Company_Id; //Constant from Drupal;
       $node["product_id"] = 'miniinthebox-se-'.md5((string)$offer[17]);
       $node["title"] =  str_replace('"', "", ((string)$offer[20]));
       $node["description"] =  str_replace('"', "", ((string)$offer[11]));
       $node["image_small_url"] =  str_replace('"', "", ((string)$offer[30]));
       $node["url"] = str_replace('"', "", ((string)$offer[29]));
       $node["raw_brand"] = str_replace('"', "", ((string)$offer[1]));
       $node["new_price"] = SH::extractPrice(str_replace('"', "", ((string)$offer[23])),",");
       $node["old_price"] = SH::extractPrice(str_replace('"', "", ((string)$offer[8])),",");


   $cat = htmlspecialchars_decode(str_replace('"', "", ((string)$offer[6])));

     if(isset($cat) && !empty($cat)){
       
       $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(htmlspecialchars_decode(str_replace('"', "", ((string)$offer[6])))),'UTF-8'));
     
       $c = json_decode(file_get_contents($catpath));
    
         $node["raw_categories"] =  htmlspecialchars_decode(str_replace('"', "", ((string)$offer[6])));

            if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
             $ids =  explode(',', $c[0]->tax_group);
             foreach($ids as $id){
               $node["categories"][] = (int) $id;
             }
        }

      }
      return $node;
    }
    return NULL;
  }

 public function ExternalData(array &$node, array $html){

  }

}
