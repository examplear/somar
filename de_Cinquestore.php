<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class de_Cinquestore extends BaseScraper {

  const Company_Id = 8800303;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_Cinquestore");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }


    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DE";
    $node["product_id"] = 'cinquestore-de-'.(string)$offer->product_id.'-'.substr(md5((string)$offer->deeplink),0,5);
    $node["url"] = utf8_decode((string)$offer->deeplink);
    $node["title"] = utf8_decode(trim((string)$offer->product_name));
    $node["raw_brand"] = utf8_decode((string)$offer->brand);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->normal_price,",");
    $node["image_small_url"] = (string)$offer->image_url;
    $node["description"] = utf8_decode((string)$offer->description);
    $node["raw_sizes"] = [utf8_decode((string)$offer->size)];
    $node["raw_colors"] = [utf8_decode((string)$offer->Colour)];
    //$node["summary"] = utf8_decode(SH::text_summary((string)$offer->longDescription));
    
    if ((string)$offer->gender =="female") {
      $node["gender"] = 1;
      $node["raw_categories"] = utf8_decode((string)$offer->merchant_category.' - female');
    } else if ((string)$offer->gender =="male") {
      $node["gender"] = 0;
      $node["raw_categories"] = utf8_decode((string)$offer->merchant_category.' - male');

    }else if ((string)$offer->gender =="unisex") {
      $node["gender"] = 2;
      $node["raw_categories"] = utf8_decode((string)$offer->merchant_category.' - unisex');
    }
    else {
      $node["gender"] = NULL;
      $node["raw_categories"] = utf8_decode((string)$offer->merchant_category.' - ');
    }


  if(isset($offer->merchant_category) && !empty($offer->merchant_category)){
      $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=de&name_hash='.md5(mb_strtolower(trim($node["raw_categories"]),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

   //  $node["raw_categories"] = utf8_decode((string)$offer->merchant_category);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
