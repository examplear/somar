<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class no_Sportamore extends BaseScraper {

  const Company_Id = 5548289;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "no");
    $this->reader = new XML2Reader(parent::GetUrl(),"no_Sportamore");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "NOK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "NO";
    $node["product_id"] = 'sportamore-no-'.(string)$offer->TDProductId.'-'.substr(md5((string)$offer->productUrl),0,5);
    $node["url"] = utf8_decode((string)$offer->productUrl);
    $node["title"] = htmlspecialchars_decode(trim((string)$offer->name));
    $node["raw_brand"] = htmlspecialchars_decode((string)$offer->brand);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->fields->retailprice,",");
    $node["image_small_url"] = (string)$offer->imageUrl;
    $node["raw_sizes"] = [utf8_decode((string)$offer->size)];
    $node["raw_colors"] = [htmlspecialchars_decode((string)$offer->fields->color)];

   $des = htmlspecialchars_decode((string)$offer->description);
   if(!empty($des)){
      $node["description"] = $des;
   }
   else
   {
      $node["description"]  =  htmlspecialchars_decode(trim((string)$offer->name));
   }

    if ((string)$offer->fields->gender =="female") {
     $node["gender"] = 1;
     $node["raw_categories"] = htmlspecialchars_decode((string)$offer->merchantCategoryName.' - female');

    } else if ((string)$offer->fields->gender =="male") {
      $node["gender"] = 0;
      $node["raw_categories"] = htmlspecialchars_decode((string)$offer->merchantCategoryName.' - male');

    }else if ((string)$offer->fields->gender =="unisex") {
      $node["gender"] = 2;
      $node["raw_categories"] = htmlspecialchars_decode((string)$offer->merchantCategoryName.' - unisex');
    }
    else if ((string)$offer->fields->gender =="none") { 
      $node["gender"] = NULL; 
      $node["raw_categories"] = htmlspecialchars_decode((string)$offer->merchantCategoryName.' - none');
    }
    else {
      $node["gender"] = NULL;      
      $node["raw_categories"] = htmlspecialchars_decode((string)$offer->merchantCategoryName);
    }


  if(isset($offer->merchantCategoryName) && !empty($offer->merchantCategoryName)){

  $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=no&name_hash='.md5(mb_strtolower(trim($node["raw_categories"]),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }
    }
    return $node;
  }

   public function ExternalData(array &$node, array $html){

  }

}