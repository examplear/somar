<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class de_Deallx extends BaseScraper {

  const Company_Id = 8423020;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_Deallx");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    
    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DE";
    $node["product_id"] = 'deallx-de-'.(string)$offer->ID.'-'.substr(md5((string)$offer->URL),0,5);
    $node["url"] = utf8_decode((string)$offer->URL);
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->properties->brand->value);
    $node["new_price"] = SH::extractPrice((string)$offer->price->amount,",");
    $node["image_small_url"] = (string)$offer->images->image;
    //$node["description"] = utf8_decode((string)$offer->description);

   $des = utf8_decode((string)$offer->description);
   if(!empty($des)){
      $node["description"] = $des;
   }
   else
   {
      $node["description"]  =  utf8_decode(trim((string)$offer->name));
   }

    if ((string)$offer->properties->gender->value =="Damen") {
       $node["gender"] = 1;
       $node["raw_categories"] = utf8_decode((string)$offer->categories->category.' - Damen');
    } else if ((string)$offer->properties->gender->value =="Herren") {
       $node["gender"] = 0;
       $node["raw_categories"] = utf8_decode((string)$offer->categories->category.' - Herren');
    }else if ((string)$offer->properties->gender->value =="unisex") {
       $node["gender"] = 2;
       $node["raw_categories"] = utf8_decode((string)$offer->categories->category.' - unisex');
    }
    else if ((string)$offer->properties->gender->value =="H") {
       $node["gender"] = 0;
       $node["raw_categories"] = utf8_decode((string)$offer->categories->category.' - H');
     }
    else if ((string)$offer->properties->gender->value =="D") {
       $node["gender"] = 1;
       $node["raw_categories"] = utf8_decode((string)$offer->categories->category.' - D');
     }  
    else {
       $node["gender"] = NULL; 
       $node["raw_categories"] = utf8_decode((string)$offer->categories->category.' - ');           
    }
    
  if(isset($offer->categories->category) && !empty($offer->categories->category)){
      $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=de&name_hash='.md5(mb_strtolower(trim( $node["raw_categories"]),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));
  
          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }else{
      $node["categories"] = [539,563];
    }

   $url =  utf8_decode((string)$offer->URL);
   $urlvalue = urldecode(substr($url,strpos($url,'&r=http')+3));

   $split_url = explode('?utm_sourc', $urlvalue);

   $input = $split_url[0];

   $node["scrape"] = ["html" => ["url" => $input, "method" => "GET"]];

   return $node;
  }

   public function ExternalData(array &$node, array $scraped){

    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"].":".$node["scrape"]["html"]["url"]." ";
      return;
    }
    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);
    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;

      $node["old_price"] = SH::extractPrice(HH::qVal($crawler, ".price  .price-pseudo"), ",", ".");


      unset($crawler);
    }

    return $node;

  }
}
