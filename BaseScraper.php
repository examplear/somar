<?hh

use Doctrine\ORM\EntityManager;

use \Helpers\StringHelpers\StringHelpers as SH;
use Masterminds\HTML5;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\ErrorLogHandler;
use Globals\Globals as G;
use Globals\OfferFlags;

abstract class BaseScraper
{
  protected array<string,int> $statistics = [];


  public function AddEvent(string $key){
    if(isset($this->statistics[$key])){
      $this->statistics[$key] = $this->statistics[$key] + 1;
    }
    else {
      $this->statistics[$key] = 1;
    }
  }

  protected array $category_map;
  protected array $color_map = [];
  protected array $size_map = [];
  protected array $brand_map = [];
  protected Set<string> $presentable_offers;
  protected Set<string> $modified_offers;
  protected Set<string> $added_offers;
  protected int $ignored_because_unpresentable_offers = 0;
  protected int $removed_unpresentable_offers = 0;
  public Map<string,int> $new_categories;
  public Map<string,int> $new_brands;
  public Map<string,int> $new_sizes;
  public Map<string,int> $new_colors;

  public Map<string,int> $validation_errors;

  public function __construct(protected MappingService $cs, protected OfferService $os, protected Logger $logger, protected int $company_id, string $lang){

    libxml_use_internal_errors(true);
    $this->presentable_offers = Set{};
    $this->modified_offers = Set{};
    $this->added_offers = Set{};
    $this->new_categories = Map {};
    $this->new_brands = Map {};
    $this->new_sizes = Map {};
    $this->new_colors = Map {};


    $this->validation_errors = Map {"id" => 0, "title" => 0 , "description" => 0, "price" => 0, "no image" => 0, "image failed" => 0, "url" => 0,"no format" => 0};

    $this->category_map = $cs->getCategories($company_id);
    $this->color_map = $cs->getColors($lang);
    $this->size_map = $cs->getSizes();
    $this->brand_map = $cs->getBrands();
  }


  public abstract function ScrapeNext() : ?array;
  public abstract function ExternalData(array &$node, array $scraped);
  public abstract function GetRandomIndex() : int;
  public abstract function GetCurrency() : string;

  // causing issue public abstract function GetCountry() : string;

  public function GetUrl(): string {
    return G::getConfig()["urls"][get_class($this)];
  }

  public function FlagUnseenAsRemoved() : int {
    return $this->os->FlagOthersAsRemoved($this->presentable_offers, $this->company_id);
  }

  /*public function XmlScrape(SimpleXMLElement $xml, Map<string, string> $mappings, Offer $input) {
    foreach($mappings as $k => $v){
      $input->$v = $xml->$k;
    }
  }*/

  public function getModifiedCount() : int {
    return $this->modified_offers->count();
  }
  public function getAddedCount() : int {
    return $this->added_offers->count();
  }
  public function getPresentableCount() : int {
    return $this->presentable_offers->count();
  }
  public function getIgnoredBecauseUnpresentableCount() : int {
    return $this->ignored_because_unpresentable_offers;
  }
  public function getRemovedUnpresentableCount() : int {
    return $this->removed_unpresentable_offers;
  }

  public function normalize(&$node) : bool {

    //Allow module to set categories on their own.
    if(!isset($node["categories"])){
      $node["categories"] = [];
      if(isset($node["raw_categories"]) && is_string($node["raw_categories"])){
        $cat_trim = SH::normalize($node["raw_categories"]);
        if(isset($this->category_map[$cat_trim])){
          $category = $this->category_map[$cat_trim];
          if($category != NULL && is_array($category)){
            $node["categories"] = $category;
          }
        }
        else { //Offer had a category, but we did not match it
          if(!isset($this->new_categories[$cat_trim])){
            $this->new_categories[$cat_trim] = 0;
          }
          $this->new_categories[$cat_trim] = $this->new_categories[$cat_trim] + 1;
        }
      }
      else{
        $node["raw_categories"] = "";
      }
    }


    //var_dump("colors",$node["raw_colors"]);
    $node["colors"] = [];
    if(isset($node["raw_colors"]) && is_array($node["raw_colors"]) ){
      foreach($node["raw_colors"] as $color){
        $trimmed = SH::normalize($color);
        if(isset($this->color_map[$trimmed]) && !empty($this->color_map[$trimmed]) )
        {
          $node["colors"][] = $this->color_map[$trimmed];
        }
        else
        {
          $node["colors"][] = $trimmed;
          if(!isset($this->new_colors[$trimmed])){
            $this->new_colors[$trimmed] = 0;
          }
          $this->new_colors[$trimmed] = $this->new_colors[$trimmed] + 1;
        }
      }
    }

    $node["sizes"] = [];
    if(isset($node["raw_sizes"]) && is_array($node["raw_sizes"]) ){
      foreach($node["raw_sizes"] as $size){
        if(isset($size) && !empty($size) && $size!=''){
          $trimmed = SH::normalize($size);
          if(isset($this->size_map[$trimmed]) && !empty($this->size_map[$trimmed]) )
          {
            $node["sizes"][] = $this->size_map[$trimmed];
          }
          else
          {
            $node["sizes"][] = $trimmed;
            if(!isset($this->new_sizes[$trimmed])){
              $this->new_sizes[$trimmed] = 0;
            }
            $this->new_sizes[$trimmed] = $this->new_sizes[$trimmed] + 1;
          }
        }
      }
    }

    $node["brand"] = isset($node["raw_brand"]) ? $node["raw_brand"] : "";
    if(isset($node["raw_brand"]) && is_string($node["raw_brand"]) ){
      $brand = $node["raw_brand"];
      $trimmed = SH::normalize($brand);
      if(isset($this->brand_map[$trimmed]) && !empty($this->brand_map[$trimmed]) )
      {
        $node["brand"] = $this->brand_map[$trimmed];
      }
      else
      {
        if(!isset($this->new_brands[$trimmed])){
          $this->new_brands[$trimmed] = 0;
        }
        $this->new_brands[$trimmed] = $this->new_brands[$trimmed] + 1;
      }
    }

    if(!isset($node["summary"]) || empty($node["summary"])) {
      $node["summary"] = null;
    }

    if(!isset($node["country"]) || empty($node["country"])) {
      $node["country"] = 'SE';
    }

    if(!isset($node["currency"]) || empty($node["currency"])){
      $node["currency"] = $this->GetCurrency();
    }



    return true;
  }

  public function validate(&$node) : OfferFlags {
    ////var_dump($node);
    if($node == NULL){
      $valid = false;
    }

    $is_presentable = true;
    $is_valid = true;

    //Allow modules to also set reason
    if(!isset($node["reason"])){
        $node["reason"] = "";
    }

    if(!isset($node["product_id"])
    || !is_string($node["product_id"])
    || mb_strlen($node["product_id"],"UTF-8") < 4){ //lets assume that ids are at least 4 chars
      $this->validation_errors["id"] = $this->validation_errors["id"] + 1;
      $node["reason"] .= "invalidid ";
      $is_valid = false;
      $is_presentable = false;
    }

    if(!isset($node["title"])
    || !is_string($node["title"])
    || empty($node["title"]))
    {
      $this->validation_errors["title"] = $this->validation_errors["title"] + 1;
      $node["title"] = "invalid";
      $node["reason"] .= "title ";
      $is_presentable = false;
    }

    if(!isset($node["description"])
    || !is_string($node["description"])
    || empty($node["description"])){
      $this->validation_errors["description"] = $this->validation_errors["description"] + 1;
      $is_presentable = false;
      $node["reason"] .= "description ";
      $node["description"] = "invalid";
    }

    if(isset($node["accept"]) && $node["accept"]== True){
      if(empty($node["new_price"]) || !is_numeric($node["new_price"])){
          $this->validation_errors["price"] = $this->validation_errors["price"] + 1;
          $is_presentable = false;
          $node["reason"] .= "neworoldprice ";
          if(!isset($node["new_price"]) || !is_numeric($node["new_price"])){
            $node["new_price"] = "-1.00";
          }
      }

    }else{
      if(empty($node["new_price"]) || !is_numeric($node["new_price"])
        || empty($node["old_price"]) || !is_numeric($node["old_price"])
        || round($node["new_price"]) >= round($node["old_price"]) ){
          //var_dump(empty($node["new_price"]), $node["new_price"]);
          //var_dump(empty($node["old_price"]) , !is_numeric($node["old_price"]));
          //var_dump(!is_numeric($node["old_price"]),round($node["new_price"]) >= round($node["old_price"]),round($node["old_price"]),round($node["new_price"]));
          $this->validation_errors["price"] = $this->validation_errors["price"] + 1;
          $is_presentable = false;
          $node["reason"] .= "neworoldprice ";
          if(!isset($node["new_price"]) || !is_numeric($node["new_price"])){
            $node["new_price"] = "-1.00";
          }
          if(!isset($node["old_price"]) || !is_numeric($node["old_price"])){
            $node["old_price"] = "-1.00";
          }
      }
    }


    if(!isset($node["image_small_url"])
    || !is_string($node["image_small_url"])
    || !$this->ValidateImage($node["image_small_url"])
    || empty($node["image_small_url"])){
        $this->validation_errors["no image"] = $this->validation_errors["no image"] + 1;
        $node["reason"] .= "noimage ";
        $is_presentable = false;
        $node["image_small_url"] = "invalid";
    }

  /* if(!BaseScraper::check_image_exists($node["image_large_url"])){
        $this->validation_errors["image failed"] = $this->validation_errors["image failed"] + 1;
      $valid = false;
    }*/

    if(!isset($node["url"])
      || !is_string($node["url"])
      || empty($node["url"])){
        $this->validation_errors["url"] = $this->validation_errors["url"] + 1;
        $node["reason"] .= "url ";
        $is_presentable = false;
        $node["url"] = "";
    }

    if(!isset($node["format"])
    || !is_string($node["format"])
    || empty($node["format"])){
        $this->validation_errors["no format"] = $this->validation_errors["no format"] + 1;
        $node["reason"] .= "format ";
      $is_presentable = false;
      $node["format"] = "";
    }

    //Optional Values
    if(!isset($node["gender"]) || $node["gender"] < 0 || $node["gender"] > 2){
      $node["gender"] = NULL;
    }

    if(!isset($node["image_large_url"]) || empty($node["image_large_url"])){
      $node["image_large_url"] = NULL;
    }

    if($is_presentable && $is_valid){
      $node["validation"] = OfferFlags::PRESENTABLE;
    }
    else if($is_valid){
      $node["validation"] = OfferFlags::VALID;
    }
    else {
      $node["validation"] = OfferFlags::INVALID;
    }
    return $node["validation"];
  }

  public function AttemptPersist($node) : Offer {
    $dbOffer = $this->os->OfferForId($node["product_id"]);

    $updated = false;
    $added = $dbOffer->getId() == 0;

    // We only set random index once. Otherwise all products will change every time.
    if(!$dbOffer->getRandomIndex()){
      $dbOffer->setRandomIndex($this->GetRandomIndex());
    }

    if($dbOffer->getTitle() !== $node["title"]){
      if( !$added ) {
        $this->logger->addDebug("Title changed to ". $node["title"] . " from: " . $dbOffer->getTitle());
      }
      $dbOffer->setTitle($node["title"]);
      $updated = true;
    }

    if($dbOffer->getUrl() !== $node["url"]){
      if( !$added ) {
        $this->logger->addDebug("url changed to ". $node["url"] . " from: " . $dbOffer->getUrl());
      }
      $dbOffer->setUrl($node["url"]);
      $updated = true;
    }

    if($dbOffer->getNewPrice() !== $node["new_price"]){
      if( !$added ) {
        $this->logger->addDebug("new_price changed to ". $node["new_price"] . " from: " . $dbOffer->getNewPrice());
      }
      $dbOffer->setNewPrice($node["new_price"]);
      $updated = true;
    }

    if($dbOffer->getOldPrice() !== $node["old_price"]){
      if( !$added ) {
        $this->logger->addDebug("old_price changed to ". $node["old_price"] . " from: " . $dbOffer->getOldPrice());
      }
      //var_dump("price_change",$dbOffer->getOldPrice(), $node["old_price"]);
      $dbOffer->setOldPrice($node["old_price"]);
      $updated = true;
    }

    if($dbOffer->getImageLargeUrl() !== $node["image_large_url"]){
      if( !$added ) {
        $this->logger->addDebug("image_large_url changed to ". $node["image_large_url"] . " from: " . $dbOffer->getImageLargeUrl());
      }
      $dbOffer->setImageLargeUrl($node["image_large_url"]);
      $updated = true;
    }

    if($dbOffer->getImageSmallUrl() !== $node["image_small_url"]){
      if( !$added ) {
        $this->logger->addDebug("image_small_url changed to ". $node["image_small_url"] . " from: " . $dbOffer->getImageSmallUrl());
      }
      $dbOffer->setImageSmallUrl($node["image_small_url"]);
      $updated = true;
    }

    if($dbOffer->getDescription() !== $node["description"]){
      if( !$added ) {
        $this->logger->addDebug("description changed to ". $node["description"] . " from: " . $dbOffer->getDescription());
      }
      $dbOffer->setDescription($node["description"]);
      $updated = true;
    }

    if($dbOffer->getSummary() !== $node["summary"]){
      if( !$added ) {
        $this->logger->addDebug("summary changed to ". $node["summary"] . " from: " . $dbOffer->getSummary());
      }
      $dbOffer->setSummary($node["summary"]);
      $updated = true;
    }

    if($dbOffer->getFormat() !== $node["format"]){
      if( !$added ) {
        $this->logger->addDebug("format changed to ". $node["format"] . " from: " . $dbOffer->getFormat());
      }
      $dbOffer->setFormat($node["format"]);
      $updated = true;
    }

    if($dbOffer->getCompanyId() !== $node["company_id"]){
      if( !$added ) {
        $this->logger->addDebug("company_id changed to ". $node["company_id"] . " from: " . $dbOffer->getCompanyId());
      }
      $dbOffer->setCompanyId($node["company_id"]);
      $updated = true;
    }

    if($dbOffer->getBrand() !== $node["brand"]){
      if( !$added ) {
        $this->logger->addDebug("brand changed to ". $node["brand"] . " from: " . $dbOffer->getBrand());
      }
      $dbOffer->setBrand($node["brand"]);
      $updated = true;
    }

    if($dbOffer->getGender() !== $node["gender"]){
      if( !$added ) {
        $this->logger->addDebug("gender changed to ". print_r($node["gender"], true) . " from: " . $dbOffer->getGender());
      }
      $dbOffer->setGender($node["gender"]);
      $updated = true;
    }

    if($dbOffer->getColors() !== $node["colors"]){
      if( !$added ) {
        $this->logger->addDebug("colors changed to ". print_r($node["colors"],true) . " from: " . print_r($dbOffer->getColors(),true));
      }
      $dbOffer->setColors($node["colors"]);
      $updated = true;
    }

    if($dbOffer->getCategories() !== $node["categories"]){
      if( !$added ) {
        $this->logger->addDebug("categories changed to ". print_r($node["categories"],true) . " from: " . print_r($dbOffer->getCategories(),true));
      }
      $dbOffer->setCategories($node["categories"]);
      $updated = true;
    }

    if($dbOffer->getRawCategories() !== $node["raw_categories"]){
      if( !$added ) {
        $this->logger->addDebug("raw_categories changed to ". print_r($node["raw_categories"],true) . " from: " . print_r($dbOffer->getRawCategories(),true));
      }
      $dbOffer->setRawCategories($node["raw_categories"]);
      $updated = true;
    }

    if($dbOffer->getSizes() !== $node["sizes"]){
      if( !$added ) {
        $this->logger->addDebug("sizes changed to ". print_r($node["sizes"],true) . " from: " . print_r($dbOffer->getSizes(),true));
      }
      $dbOffer->setSizes($node["sizes"]);
      $updated = true;
    }

    if($dbOffer->getCurrency() !== $node["currency"]){
      if( !$added ) {
        $this->logger->addDebug("currency changed to ". print_r($node["currency"],true) . " from: " . print_r($dbOffer->getCurrency(),true));
      }
      $dbOffer->setCurrency($node["currency"]);
      $updated = true;
    }

    if($dbOffer->getCountry() !== $node["country"]){
      if( !$added ) {
        $this->logger->addDebug("country changed to ". print_r($node["country"],true) . " from: " . print_r($dbOffer->getCountry(),true));
      }

      $dbOffer->setCountry($node["country"]);
      $updated = true;
    }
    //}

    if($updated){
        $dbOffer->updateModifiedDate();
    }

    $dbOffer->ActiveInFeed();
    if($node["validation"] == OfferFlags::PRESENTABLE){
      $dbOffer->PresentableInFeed();

      $this->os->SaveOffer($dbOffer);
      $this->presentable_offers->add($dbOffer->getProductId());

      if($added){
        $this->added_offers->add($dbOffer->getProductId());
        $this->logger->addNotice("Adding new offer:                   " . $node["product_id"]);
      }
      else if($updated){
        $this->modified_offers->add($dbOffer->getProductId());
        $this->logger->addNotice("Updating existing offer product_id: " . $node["product_id"] . " id: " . $dbOffer->getId());
      }
      else {
        $this->logger->addNotice("Marking as active in feed:          " . $node["product_id"] . " id: " . $dbOffer->getId());
      }

    }
    else {

      if($dbOffer->getId() == 0){
        if($dbOffer->getState() !=2){
            $dbOffer->SetRemoved();
        }
        //Ignore this offer! We have never seen it before
        $this->ignored_because_unpresentable_offers++;
        $this->logger->addNotice("Ignoring unpresentable offer:       " . $node["product_id"], ["reason" => $node["reason"]]);
      }
      else if($node["validation"] == OfferFlags::VALID){
        if($dbOffer->getState() !=2){
          $dbOffer->SetRemoved();
          $this->os->SaveOffer($dbOffer);
          $this->logger->addNotice("Saving invalid existing offer:      " . $node["product_id"], ["reason" => $node["reason"]]);
          $this->removed_unpresentable_offers++;
        }

      }

    }

    return $dbOffer;
  }
/*
  public function CrawlerFromUrl(string $url, (function(string,Crawler): array<string,string>) $callback) : ?array {
    $data = $this->get_web_page($url);
    $this->crawler->clear();
    if($data["status"] != 200) {
      return NULL;
    }
    $this->crawler->add($data["content"]);
    return $callback($data["status"],$this->crawler);
  }
*/
 public function ValidateImage($url){
   $url = str_replace(" ","%20",$url);
   $ch = curl_init($url);
	curl_setopt($ch, CURLOPT_NOBODY, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_exec($ch);

	$filesize = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
	$filetype = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
	switch ($filetype) {
		case "image/gif":
		case "image/jpeg":
		case "image/png":
		case "image/bmp":
			$image = true;
			break;
		default:
			$image = false;
	}
	curl_close($ch);
	if ($filesize>0 && $image) return true;
  else if(getimagesize($url) && fopen(str_replace(" ","%20",$url),'r'))  return true;
 }


  public function SplitAndJoin($string,$token){
    return $this->Join(explode($token,$string));
  }
  public function Join($iterable)  {
    $c = "";
    foreach($iterable as $val){
      $c.=trim($val);

    }
    return trim($c);

  }

  public function Combine($iterable)  {
    $cat = '';
    $a=0;
    foreach($iterable as $categor)
    {

      if($a!=0)
      {
        $cat .=' '.$categor;
      }else
      {
        $cat .=$categor;
      }
      $a++;
    }
    return trim(preg_replace('!\s+!', ' ', $cat));

  }

}
