<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
header('Content-Type: text/html; charset=utf-8');
class dk_Petdreams extends BaseScraper {

  const Company_Id = 8502148;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "dk");
    $this->reader = new XML2Reader(parent::GetUrl(),"dk_Petdreams");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "DKK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'petdreams-dk-'.substr(md5((string)$offer->URL),0,5);
    $node["country"] = "DK";
    $node["url"] = (string)$offer->URL;
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["image_small_url"] = (string)$offer->images->image;
    $node["description"] = utf8_decode((string)$offer->description);
    $node["summary"] = utf8_decode(SH::text_summary((string)$offer->description));
    $node["raw_colors"] = [(string)$offer->color];
    $node["raw_sizes"] = [(string)$offer->size];

    
    $url = (string)$offer->URL;
    $pos = strrpos($url,'http');
    
    $node["scrape"] = ["html" => ["url" => urldecode(substr($url,$pos)), "method" => "GET"]];



    if(isset($offer->categories->category) && !empty($offer->categories->category)){
        $catpath = 'http://beta.smartster.no/driver.php?table=ImportStrings&method=search_hash&country=dk&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->categories->category)),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

     $node["raw_categories"] = utf8_decode((string)$offer->categories->category);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }

  return $node;
  }

  public function ExternalData(array &$node, array $scraped){
    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"].":".$node["scrape"]["html"]["url"]." ";
      return;
    }
    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);
    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;

     $node["old_price"] = SH::extractPrice(HH::qVal($crawler, ".product-primary-column .price-box .old-price .price"), "", ".");

     $node["new_price"] = SH::extractPrice(HH::qVal($crawler, ".product-primary-column .price-box .special-price .price"), "", ".");
      unset($crawler);
    }

    return $node;
  }

}
