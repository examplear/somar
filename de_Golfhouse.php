<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class de_Golfhouse extends BaseScraper {

  const Company_Id = 8330676;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_Golfhouse");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DE";
    $node["product_id"] = 'golfhouse-de-'.(string)$offer['zupid'].'-'.substr(md5((string)$offer->deepLink),0,5);
    $node["url"] = utf8_decode((string)$offer->deepLink);
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->manufacturer);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->oldPrice,",");
    $node["image_small_url"] = (string)$offer->mediumImage;
    $node["image_large_url"] = (string)$offer->largeImage;
    $node["raw_sizes"] = explode("/",(string)$offer->size);
    $node["raw_colors"] = [utf8_decode((string)$offer->color)];
    $node["description"] = utf8_decode((string)$offer->longDescription);    

    if ((string)$offer->gender =="female") {
      $node["gender"] = 1;
      $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath.' - female');

    } else if ((string)$offer->gender =="male") {
      $node["gender"] = 0;
      $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath.' - male');

    }else if ((string)$offer->gender =="unisex") {
      $node["gender"] = 2;
      $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath.' - unisex');
    }
    else {
      $node["gender"] = NULL;
      $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath);
    }

  if(isset($offer->merchantCategoryPath) && !empty($offer->merchantCategoryPath)){
      $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=de&name_hash='.md5(mb_strtolower(trim($node["raw_categories"]),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
