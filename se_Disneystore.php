<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class se_Disneystore extends BaseScraper {

  const Company_Id = 9537516;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Disneystore");
  }
  public function GetRandomIndex(): int {
      return rand(10000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'disneystore-se-'.md5((string)$offer->SKU.(string)$offer->TrackingUrl);
    $node["url"] = (string)$offer->TrackingUrl;
    $node["new_price"] = SH::extractPrice((string)$offer->Price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->OriginalPrice,",");
    $node["title"] = utf8_decode(trim((string)$offer->Name));
    $node["image_small_url"] = (string)$offer->ImageUrl;
    $node["description"] = utf8_decode((string)$offer->Description);
    $node["raw_brand"] = utf8_decode((string)$offer->Brand);
   // $node["raw_sizes"] = [utf8_decode((string)$offer->size)];
    //$node["raw_colors"] = [utf8_decode((string)$offer->color)];

    $nam1 =   $offer->Extras->Name;
    $val1 =  $offer->Extras->Value;

    if($nam1[0] == 'COLOUR'){
        $col1 = $val1[0];
    }
    else if($nam1[1] == 'COLOUR'){
        $col1 = $val1[1];
    }
    else if($nam1[2] == 'COLOUR'){
        $col1 = $val1[2];
    }
    else if($nam1[3] == 'COLOUR'){
        $col1 = $val1[3];
    }
    else{
      $col1 = '';
    }

 $node["raw_colors"] = [utf8_decode((string)$col1)];

    $nam2 =   $offer->Extras->Name;
    $val2 =   $offer->Extras->Value;

    if($nam2[0] == 'SIZE'){
        $size2 = $val2[0];
    }
    else if($nam2[1] == 'SIZE'){
        $size2 = $val2[1];
    }
    else if($nam2[2] == 'SIZE'){
        $size2 = $val2[2];
    }
    else if($nam2[3] == 'SIZE'){
        $size2 = $val2[3];
    }
    else{
      $size2 = '';
    }

    $node["raw_sizes"] = [utf8_decode((string)$size2)];

    if(isset($offer->Category) && !empty($offer->Category)){
        $catpath = 'http://beta.smartster.no/driver.php?table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->Category)),'UTF-8'));

        $c = json_decode(file_get_contents($catpath));

        $node["raw_categories"] = utf8_decode((string)$offer->Category);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
         $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }

    return $node;
  }

   public function ExternalData(array &$node, array $html){

  }

}
