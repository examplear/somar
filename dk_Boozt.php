<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class dk_Boozt extends BaseScraper {

  const Company_Id = 1041386;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "dk");
    $this->reader = new XML2Reader(parent::GetUrl(),"dk_Boozt");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "DKK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DK";
    $node["product_id"] = 'boozt-dk-'.(string)$offer->fields->product_id.'-'.md5((string)$offer->productUrl);
    $node["url"] = utf8_decode((string)$offer->productUrl);
    $node["title"] = htmlspecialchars_decode((string)$offer->name);
    $node["raw_brand"] = htmlspecialchars_decode((string)$offer->brand);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->previousPrice,",");
    $node["image_small_url"] = (string)$offer->imageUrl;
    $node["raw_sizes"] =  array_slice(explode(",",(string)$offer->fields->size),0,6);    
    $node["raw_colors"] = array_slice(explode("#",(string)$offer->fields->colours),0,6);

    //$node["description"] = utf8_decode((string)$offer->description);

   $des = htmlspecialchars_decode((string)$offer->description);
   if(!empty($des)){
      $node["description"] = $des;
   }
   else
   {
      $node["description"]  =  htmlspecialchars_decode((string)$offer->name);
   }

    if ((string)$offer->fields->gender =="women") {
      $node["gender"] = 1;

    } else if ((string)$offer->fields->gender =="men") {
      $node["gender"] = 0;

    }
    else if ((string)$offer->fields->gender =="unisex") {
      $node["gender"] = 2;
    }
    else {
      $node["gender"] = NULL;
    }

    if(isset($offer->fields->subCategoryPath) && !empty($offer->fields->subCategoryPath)){
     $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=dk&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->fields->subCategoryPath)),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

       $node["raw_categories"] = utf8_decode((string)$offer->fields->subCategoryPath);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

public function ExternalData(array &$node, array $html){

  }
}
