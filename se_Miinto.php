<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
header('Content-Type: text/html; charset=utf-8');
class se_Miinto extends BaseScraper {

  const Company_Id = 4016328;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Miinto");
  }
  public function GetRandomIndex(): int {
      return rand(30000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'miinto-se-'.(string)$offer['zupid'].'-'.substr(md5((string)$offer->deepLink),0,5);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->oldPrice,",");
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->manufacturer);
    $node["image_small_url"] = (string)$offer->largeImage;
    $node["url"] = (string)$offer->deepLink;
    $node["raw_sizes"] = [utf8_decode((string)$offer->size)];
    $node["raw_colors"] = [utf8_decode((string)$offer->color)];
  //  $node["description"] = utf8_decode((string)$offer->description);
   
   $des = utf8_decode((string)$offer->description);
   if(!empty($des)){
      $node["description"] = $des;
   }
   else
   {
      $node["description"]  =  utf8_decode((string)$offer->name);
   }

    if ((string)$offer->gender =="Female, , Adult") {
      $node["gender"] = 1;
      $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath.' - Female, Adult');

    } else if ((string)$offer->gender =="Male, , Adult") {
      $node["gender"] = 0;
      $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath.' - Male, Adult');
    }
    else if ((string)$offer->gender =="unisex, , Adult") {
      $node["gender"] = 2;
      $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath.' - Unisex, Adult');
    }
    else {
      $node["gender"] = NULL;
      $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath);
    }

    if(isset($offer->merchantCategoryPath) && !empty($offer->merchantCategoryPath)){
        $catpath = 'http://beta.smartster.no/driver.php?table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim($node["raw_categories"]),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

   //  $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }

    return $node;
  }

   public function ExternalData(array &$node, array $html){

  }


}
