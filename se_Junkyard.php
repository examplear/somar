<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
class se_Junkyard extends BaseScraper {

  const Company_Id = 9636927;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Junkyard");
  }
  public function GetRandomIndex(): int {
      return rand(70000,10000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'junkyard-se-'.md5((string)$offer->SKU.(string)$offer->TrackingUrl);
    $node["url"] = utf8_decode((string)$offer->TrackingUrl);
    $node["title"] = utf8_decode(trim((string)$offer->Name));
    $node["raw_brand"] = utf8_decode((string)$offer->Brand);
    $node["new_price"] = SH::extractPrice((string)$offer->Price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->OriginalPrice,",");
    $node["image_small_url"] = (string)$offer->ImageUrl;
    $node["description"] = utf8_decode((string)$offer->Description);

    $nam1 =  $offer->Extras->Name;
    $val1 =  $offer->Extras->Value;

    if($nam1[0] == 'COLOUR'){
        $col1 = $val1[0];
    }
    else if($nam1[1] == 'COLOUR'){
        $col1 = $val1[1];
    }
    else{
      $col1 = '';
    }

    $node["raw_colors"] = explode(' ', $col1);


    $nam =   $offer->Extras->Name;
    $val =  $offer->Extras->Value;

    if($nam[0] == 'GENDER'){
        $gen = $val[0];
    }
    else if($nam[1] == 'GENDER'){
        $gen = $val[1];
    }
    else{
      $gen = '';
    }

    if($gen == "Tjej"){
      $node["gender"] = 1;
      $node["raw_categories"] = utf8_decode((string)$offer->Category.' - Tjej');

    } elseif($gen == "Kille"){
      $node["gender"] = 0;
       $node["raw_categories"] = utf8_decode((string)$offer->Category.' - Kille');
    }
    elseif($gen == "Kille,Tjej"){
       $node["gender"] = 2;
       $node["raw_categories"] = utf8_decode((string)$offer->Category.' - Kille,Tjej');
    }
    else {
     $node["gender"] = NULL;
     $node["raw_categories"] = utf8_decode((string)$offer->Category);
    }

    if(isset($offer->Category) && !empty($offer->Category)){
        $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim($node["raw_categories"]),'UTF-8'));

        $c = json_decode(file_get_contents($catpath));

      //  $node["raw_categories"] = utf8_decode((string)$offer->Category);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
         $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

   public function ExternalData(array &$node, array $html){

  }

}
