<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Monolog\Logger;

class se_Bamba extends BaseScraper {

  const Company_Id = 6682812;

  protected CSVReader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new CSVReader(parent::GetUrl(),"se_bamba","\t");
  }
  public function GetRandomIndex(): int {
      return rand(1, 100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {

    // loop through the file line-by-line
    if(($data = $this->reader->Next()) === NULL){
      return NULL;
    }

    $offer = $data;
    // resort/rewrite data and insert into DB here
    // try to use conditions sparingly here, as those will cause slow-performance
    if($offer && isset($offer[1])){

      $node = [];

      /*if((string)$offer[10] == 'Ja'){
        $node["new_price"] = SH::extractPrice((string)$offer[2],",");
      }else{
        $node["new_price"] = '';
      }*/

      $node["format"] = "full_html";
      $node["company_id"] = self::Company_Id; //Constant from Drupal;

      $node["product_id"] = 'bamba-'.md5((string)$offer[3]);
      $node["title"] = utf8_encode((string)$offer[4]);


      $node["description"] = utf8_encode(trim((string)$offer[8]));
      $node["summary"] = SH::text_summary(utf8_encode(trim((string)$offer[8])));
      $node["raw_categories"] = utf8_encode((string)$offer[0]);

      $node["image_small_url"] = utf8_encode((string)$offer[9]);

      $node["url"] = (string)$offer[3].'?utm_source=Smartster.se&utm_medium=Smartster&utm_term=Smartster&utm_content=Smartster&utm_campaign=Smartster';
      $node["raw_brand"] = utf8_encode((string)$offer[6]);



      $node["scrape"] = ["html" => ["url" => (string)$offer[3], "method" => "GET"]];


      unset($data);
      return $node;
    }
    return NULL;
  }
  public function ExternalData(array &$node, array $scraped){
    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"].":".$node["scrape"]["html"]["url"]." ";
      return;
    }
    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);
    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;

      $o_price = HH::querySelector($crawler, ".vargroupingtag .price_procent");
      if($o_price === NULL){
        $o_price = "";
        $node["old_price"] = $o_price;
      }else {
        $o_price = $o_price->nodeValue;
        $node["old_price"] = SH::extractPrice($o_price, ",");
      }

      unset($crawler);
    }

    return $node;
  }
}
