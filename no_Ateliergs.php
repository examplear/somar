<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class no_Ateliergs extends BaseScraper {

  const Company_Id = 5577176;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "no");
    $this->reader = new XML2Reader(parent::GetUrl(),"no_Ateliergs");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "NOK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "NO";
    $node["product_id"] = 'ateliergs-no-'.(string)$offer->TDProductId.'-'.substr(md5((string)$offer->productUrl),0,5);
    $node["url"] = utf8_decode((string)$offer->productUrl);
    $node["title"] = htmlspecialchars_decode(trim((string)$offer->name));
    $node["raw_brand"] = htmlspecialchars_decode((string)$offer->fields->brand);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->fields->sale_price,",");
    $node["image_small_url"] = (string)$offer->imageUrl;
    $node["description"] = htmlspecialchars_decode((string)$offer->description);
    $node["raw_colors"] = [htmlspecialchars_decode((string)$offer->fields->color)];
    $node["raw_sizes"] = explode(",",(string)$offer->fields->Size);

   if ((string)$offer->fields->Gender =="FEMALE") {
      $node["gender"] = 1;

    } else if ((string)$offer->fields->Gender =="MALE") {
      $node["gender"] = 0;

    }else if ((string)$offer->fields->Gender =="Unisex") {
      $node["gender"] = 2;
    }
    else {
      $node["gender"] = NULL;
    }

  if(isset($offer->fields->product_type) && !empty($offer->fields->product_type)){

  $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=no&name_hash='.md5(mb_strtolower(trim(htmlspecialchars_decode((string)$offer->fields->product_type)),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

     $node["raw_categories"] = htmlspecialchars_decode((string)$offer->fields->product_type);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }
    }
    return $node;
  }

   public function ExternalData(array &$node, array $html){

  }

}