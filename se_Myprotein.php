<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class se_Myprotein extends BaseScraper {

  const Company_Id = 8213342;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Myprotein");
  }
  public function GetRandomIndex(): int {
      return rand(30000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'myprotein-'.(string)$offer['zupid'];
    $node["url"] = (string)$offer->deepLink;
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->oldPrice,",");
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["image_small_url"] = (string)$offer->mediumImage;
    $node["image_large_url"] = (string)$offer->largeImage;
    $node["raw_sizes"] = [(string)$offer->size];
    $node["raw_colors"] = [(string)$offer->color];
    $node["description"] = utf8_decode((string)$offer->description);
    $node["summary"] = utf8_decode((string)$offer->longDescription); // TODO
    $node["raw_brand"] = utf8_decode((string)$offer->manufacturer);
    $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath);


    if ((string)$offer->gender == "Kvinnor") {
      $node["gender"] = 1;

    } else if ((string)$offer->gender == "Män") {
      $node["gender"] = 0;
    }

    else if ((string)$offer->gender == "Män, Kvinnor") {
      $node["gender"] = 2;
    }
    else {
      $node["gender"] = NULL;
    }

    return $node;
  }

   public function ExternalData(array &$node, array $html){

  }


}
