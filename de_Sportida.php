<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
 header('Content-Type: text/html; charset=utf-8');
class de_Sportida extends BaseScraper {

  const Company_Id = 9270915;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_Sportida");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DE";
    $node["product_id"] = 'sportida-de-'.(string)$offer->Details->ProductID.'-'.substr(md5((string)$offer->Deeplinks->Product),0,5);
    $node["url"] = htmlspecialchars_decode((string)$offer->Deeplinks->Product);
    $node["title"] = htmlspecialchars_decode(trim((string)$offer->Details->Title));
    $node["raw_brand"] = htmlspecialchars_decode((string)$offer->Details->Brand);
    $node["new_price"] = SH::extractPrice((string)$offer->Price->Price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->Price->PriceOld,",");
    $node["image_small_url"] = (string)$offer->Images->Img[1]->URL;

   $node["description"] = trim(preg_replace('/\s\s+/', ' ', str_replace(" â", " –", htmlspecialchars((string)$offer->Details->Description))));

    $node["raw_sizes"]  = [utf8_decode((string)$offer->Properties->Property[0]->attributes()->Text)];
    $node["raw_colors"] = [utf8_decode((string)$offer->Properties->Property[2]->attributes()->Text)];   

  $val = htmlspecialchars_decode((string)$offer->CategoryPath->ProductCategoryPath);

   if (strpos($val,'Herren') !== false) {
      $node["gender"] = 0;
   }
   else if (strpos($val,'Damen') !== false) {
      $node["gender"] = 1;
   }
   else if (strpos($val,'Unisex') !== false) {
      $node["gender"] = 2;
   }else{
      $node["gender"] = null;
   }

  if(isset($offer->CategoryPath->ProductCategoryPath) && !empty($offer->CategoryPath->ProductCategoryPath)){
  
      $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=de&name_hash='.md5(mb_strtolower(trim(htmlspecialchars_decode((string)$offer->CategoryPath->ProductCategoryPath)),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

     $node["raw_categories"] = htmlspecialchars_decode((string)$offer->CategoryPath->ProductCategoryPath);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
