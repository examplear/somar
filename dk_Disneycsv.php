<?hh

use \Helpers\StringHelpers\StringHelpers as SH;
use Underscore\Underscore as _;
use Monolog\Logger;

class dk_Disneycsv extends BaseScraper {

  const Company_Id = 1112223;

  protected CSVReader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new CSVReader(parent::GetUrl(),"dk_Disneycsv","\t");
  }
  public function GetRandomIndex(): int {
      return rand(40000, 100000);
  }
  public function GetCurrency() : string {
    return "DKK";
  }
  public function ScrapeNext() : ?array {

    
    if(($data = $this->reader->Next()) === NULL){
      return NULL;
    }

    $offer = $data;
    
    if($offer && isset($offer[0])){
      $node = [];
      $node["format"] = "full_html";
      $node["company_id"] = self::Company_Id;
      $node["image_small_url"] = (string)$offer[9];
      $node["product_id"] = 'dk_disneycsv-'.(string)$offer[0];
      $node["title"] = (string)$offer[1];
      $node["new_price"] = SH::extractPrice((string)$offer[4],",");
      $node["old_price"] = SH::extractPrice((string)$offer[12],",");
      $node["url"] = (string)$offer[8];
      $node["description"] = (string)$offer[2];
      $node["raw_brand"] = (string)$offer[11];
      $node["raw_extras"] = (string)$offer[15];

      if(isset($offer[3]) && !empty($offer[3])){
          $catpath = 'http://localhost/smartster-categorizer/driver.php?t=?table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(((string)$offer[3])),'UTF-8'));

          $c = json_decode(file_get_contents($catpath));
          $node["raw_categories"] = (string)$offer[3];
          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
             $ids =  explode(',', $c[0]->tax_group);
               foreach($ids as $id){
                 $node["categories"][] = (int) $id;
               }
          }
      }


      unset($data);
      return $node;
    }
    return NULL;
  }
  public function ExternalData(array &$node, array $html){

  }
}
