<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Monolog\Logger;


class se_Vaskan extends BaseScraper {

  const Company_Id = 8879109;

  protected CSVReader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new CSVReader(parent::GetUrl(),"se_Vaskan",";");
  }
  public function GetRandomIndex(): int {
      return rand(1, 100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {

    // loop through the file line-by-line
    if(($data = $this->reader->Next()) === NULL){
      return NULL;
    }

    $offer = $data;
    // resort/rewrite data and insert into DB here
    // try to use conditions sparingly here, as those will cause slow-performance
    if($offer && isset($offer[0])){
      
      $node = [];

      //$node["new_price"] = SH::extractPrice((string)$offer[5],",");
      $node["format"] = "full_html";
      $node["company_id"] = self::Company_Id; //Constant from Drupal;
      //$node["product_id"] = 'vaskan-'.md5((string)$offer[1]);
      $node["product_id"] = 'vaskan-se-'.(string)$offer[0].'-'.substr(md5((string)$offer[11]),0,5);
      $node["title"] = utf8_encode((string)$offer[1]);
      $node["description"] = trim(utf8_encode((string)$offer[2]));
      $node["summary"] = SH::text_summary(utf8_encode((trim((string)$offer[2]))));
      $node["raw_categories"] = utf8_encode((string)$offer[4]);
      
      $node["image_small_url"] = (string)$offer[10];
          //$node["raw_sizes"] = [(string)$offer->size];

      $node["url"] = str_replace("prisjakt", "smartster",(string)$offer[11]);


      $node["scrape"] = ["html" => ["url" => (string)$offer[11], "method" => "GET"]];


    if(isset($offer[4]) && !empty($offer[4])){
        $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(utf8_encode((string)$offer[4])),'UTF-8'));
      
     $c = json_decode(file_get_contents($catpath));
  
     
          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }

      unset($data);
      return $node;
    }
    return NULL;
  }


  public function ExternalData(array &$node, array $scraped){
    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"].":".$node["scrape"]["html"]["url"]." ";
      return;
    }
    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);
    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;
      
      
      //$node["old_price"] = "500"; 
      //$node["new_price"] = "300"; 


      $node["old_price"] = SH::extractPrice(HH::qVal($crawler, "#articlePriceComponent #regularPrice"), "", "."); 

      $node["new_price"] = SH::extractPrice(HH::qVal($crawler, "#articlePriceComponent #price .reducedPrice"), "", "."); 

      unset($crawler);
    }

    return $node;
  }
}
