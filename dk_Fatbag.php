<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class dk_Fatbag extends BaseScraper {

  const Company_Id = 8709763;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "dk");
    $this->reader = new XML2Reader(parent::GetUrl(),"dk_Fatbag");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "DKK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DK";
    $node["product_id"] = 'fatbag-dk-'.(string)$offer->id.'-'.md5((string)$offer->link);
    $node["url"] = utf8_decode((string)$offer->link);
    $node["title"] = utf8_decode((string)$offer->title);
    $node["raw_brand"] = utf8_decode((string)$offer->brand);
    $node["new_price"] = SH::extractPrice((string)$offer->prices->salesprice,",");
    $node["old_price"] = SH::extractPrice((string)$offer->prices->marketprice,",");
    $node["image_small_url"] = (string)$offer->image;
    $node["description"] = utf8_decode((string)$offer->description);

  if ((string)$offer->genders =="Kvinde") {
      $node["gender"] = 1;
      $node["raw_categories"] = utf8_decode((string)$offer->category.' - Kvinde');
    } else if ((string)$offer->genders =="Mand") {
      $node["gender"] = 0;
      $node["raw_categories"] = utf8_decode((string)$offer->category.' - Mand');

    }else if ((string)$offer->genders =="Mand,Kvinde") {
      $node["gender"] = 2;
      $node["raw_categories"] = utf8_decode((string)$offer->category.' - Mand,Kvinde');
    }
    else {
      $node["gender"] = NULL;
      $node["raw_categories"] = utf8_decode((string)$offer->category);
    }

    if(isset($offer->category) && !empty($offer->category)){
     $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=dk&name_hash='.md5(mb_strtolower(trim($node["raw_categories"]),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

public function ExternalData(array &$node, array $html){

  }
}
