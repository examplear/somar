<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;

class se_BiliaBil extends BaseScraper {

  //const MQ_URL = "/vagrant/www/mq_demo_2.xml";
  const Company_Id = 2521125;
  const Base_Url = "http://www.bilia.se";
  private GenericWebReader $reader;
  private $generator;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new GenericWebReader([[
      "url"=> parent::GetUrl(),
      "options" => ["timeout" => 1000], // seconds
      "selector" => ".stock-vehicles li a.showcar",
      "callback" => ($crawler, $node)
        ==> ( HH::querySelector($crawler, ".price--previous", $node) ) !== null
          ? $node->getAttribute("href")
          : ""]]);
    $this->generator = $this->reader->GetGenerator();
    $this->generator->rewind();

  }
  public function GetRandomIndex(): int {
      return rand(50000, 100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }

  public function ScrapeNext() : ?array {

    if(!$this->generator->valid()){
      return NULL;
    }

    $url = $this->generator->current();
    $this->generator->next();


    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["__product_url"] = $url;
    //$page = parent::get_web_page((string)$offer->advertiserProductUrl);
    $node["product_id"] = 'biliabil-'.md5($node["__product_url"]);
    $node["scrape"] = ["html" => ["url" => self::Base_Url.$url, "method" => "GET"]];

    return $node;
  }

  public function ExternalData(array &$node, array $scraped){

    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"]." ";
      return;
    }

    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);

    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;

      $node["title"] = HH::qVal($crawler, ".vehicle__summary h2");

    	$node["raw_brand"] = explode(' ',trim($node["title"]))[0];
      $node["new_price"] = SH::extractPrice(HH::qVal($crawler, ".vehicle__prices .price--current>.vat span"), "", ".");
      $node["old_price"] = SH::extractPrice(HH::qVal($crawler, ".vehicle__prices .price--previous>.vat span"), "", ".");

      $node["description"] = HH::htmlFromSelector($crawler,".vehicle__summary .vehicle-summary__table");
			$node["image_small_url"] = HH::qAttr($crawler,".image-gallery picture img",'src');
      $node["image_small_url"] = str_replace('https','http',$node["image_small_url"]);
      $node["url"] = self::Base_Url.$node["__product_url"];

      $node["summary"] = SH::text_summary($node["description"]);
      $node["categories"] = [18,512,585];
      $node["raw_categories"] = "";
      unset($crawler);
    }

    return $node;
  }


}
