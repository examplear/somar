<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class de_Crocs extends BaseScraper {

  const Company_Id = 7755488;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_Crocs");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DE";
    $node["product_id"] = 'crocs-de-'.(string)$offer['zupid'];
    $node["url"] = (string)$offer->deepLink;
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->manufacturer);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->oldPrice,",");

    $node["image_small_url"] = (string)$offer->mediumImage;
    $node["image_large_url"] = (string)$offer->largeImage;

    if ((string)$offer->gender =="Damen" || (string)$offer->gender =="Mädchen") {
      $node["gender"] = 1;

    } else if ((string)$offer->gender =="Herren" || (string)$offer->gender =="Jungen") {
      $node["gender"] = 0;

    }else if ((string)$offer->gender =="Unisex") {
      $node["gender"] = 2;
    }
    else {
      $node["gender"] = NULL;
    }

    $node["raw_colors"] = [utf8_decode((string)$offer->color)];
    $node["raw_sizes"] = [(string)$offer->size];
    $node["description"] = utf8_decode((string)$offer->longDescription);
    $node["summary"] = utf8_decode(SH::text_summary((string)$offer->name));

  if ((string)$offer->gender == "Damen") {
      $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath.' - Damen');
    } else if ((string)$offer->gender == "Herren") {
      $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath.' - Herren');
    } else if ((string)$offer->gender == "Unisex") {
      $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath.' - Unisex');
    }
    else if ((string)$offer->gender == "Mädchen") {
      $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath.' - Mädchen');
    }
    else if ((string)$offer->gender == "Jungen") {
      $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath.' - Jungen');
    }

    else {
      $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath.' - ');
    }

    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
