<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
class se_Jotex extends BaseScraper {

  const Company_Id = 8979875;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Jotex");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'jotex-se-'.(string)$offer->TDProductId.'-'.substr(md5((string)$offer->productUrl),0,5);
    $node["url"] = (string)$offer->productUrl;
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->previousPrice,",");
    $node["title"] = (trim((string)$offer->name));
    $node["image_small_url"] = (string)$offer->imageUrl;
    $node["raw_sizes"] = [(string)$offer->size];
    $node["description"] = htmlspecialchars_decode((string)$offer->description);
    $node["raw_brand"] = utf8_decode((string)$offer->brand);


    if(isset($offer->merchantCategoryName) && !empty($offer->merchantCategoryName)){
        $catpath = 'http://beta.smartster.no/driver.php?table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(htmlspecialchars_decode((string)$offer->merchantCategoryName)),'UTF-8'));

        $c = json_decode(file_get_contents($catpath));

        $node["raw_categories"] = htmlspecialchars_decode((string)$offer->merchantCategoryName);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
         $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }



    return $node;
  }

   public function ExternalData(array &$node, array $html){

  }


}
