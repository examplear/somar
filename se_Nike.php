<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Monolog\Logger;

class se_Nike extends BaseScraper {


  const Company_Id = 5421278;

  protected CSVReader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new CSVReader(parent::GetUrl(),"se_nike","\t");
  }
  public function GetRandomIndex(): int {
      return rand(10000, 100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }

  public function ScrapeNext() : ?array {

    // loop through the file line-by-line
    if(($data = $this->reader->Next()) === NULL){
      return NULL;
    }

    $offer = $data;
    // resort/rewrite data and insert into DB here
    // try to use conditions sparingly here, as those will cause slow-performance
    if($offer && isset($offer[0])){

      $node = [];

      $node["format"] = "full_html";
     // $node["country"] = "SE";
      $node["company_id"] = self::Company_Id; //Constant from Drupal;
      $node["product_id"] = 'nike-'.md5((string)$offer[8]);
      $node["title"] = str_replace('"', "", ((string)$offer[9]));
      $node["description"] = str_replace('"', "", ((string)$offer[1]));
      $node["summary"] = str_replace('"', "", ((string)$offer[1]));
      $node["raw_categories"] = str_replace('"', "", ((string)$offer[6]));
      $node["url"] = (string)$offer[3];
      $node["new_price"] = SH::extractPrice((string)$offer[7],",");
      $node["old_price"] = SH::extractPrice((string)$offer[18],",");
      $node["raw_brand"] = (string)$offer[34];
      $node["raw_colors"] = $offer[19];
      $node["raw_sizes"] = explode("|",(string)$offer[29]);
      $node["image_small_url"] = trim((string)$offer[2]);

      if ($node["gender"] = $offer[24] == "Female") {
          $node["gender"] = 1;
      } elseif ($node["gender"] = $offer[24] == "Male") {
          $node["gender"] = 0;
      } else {
          $node["gender"] = NULL;
      }


      unset($data);
      return $node;
    }
    return NULL;
  }
  public function ExternalData(array &$node, array $scraped){
  }
}
