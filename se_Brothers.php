<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class se_Brothers extends BaseScraper {

  const Company_Id = 9330207;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Brothers");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'brothers-se-'.(string)$offer->SKU.'-'.substr(md5((string)$offer->TrackingUrl),0,5);
    $node["url"] = (string)$offer->TrackingUrl;
    $node["old_price"] = SH::extractPrice((string)$offer->Price,",");
    $node["title"] = utf8_decode(trim((string)$offer->Name));
    $node["image_small_url"] = (string)$offer->ImageUrl;
    $node["raw_brand"] = utf8_decode((string)$offer->Brand);

    //$description =  preg_replace('!\s+!', ' ', str_replace('',' ',str_replace('',' ',str_replace('','',str_replace('',' ',str_replace('Â',' ',$offer->Description))))));
    //$node["description"] =  utf8_encode(preg_replace('!\s+!', ' ',mb_convert_encoding($offer->Description,"HTML-ENTITIES","UTF-8")));
    $node["description"] =  utf8_decode(htmlspecialchars(preg_replace('!\s+!', ' ',$offer->Description),ENT_QUOTES));

    if(isset($offer->Category) && !empty($offer->Category)){
      $offer->Category =   preg_replace('!\s+!', ' ', str_replace('','',str_replace('',' ',str_replace('Â',' ',$offer->Category))));

        $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->Category)),'UTF-8'));

        $c = json_decode(file_get_contents($catpath));
        $node["raw_categories"] =  utf8_decode( (string) $offer->Category);

        if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
         $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }

    $node["scrape"] = ["html" => ["url" => (string) $offer->ProductUrl, "method" => "GET"]];

    return $node;
  }

  public function ExternalData(array &$node, array $scraped){

    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"].":".$node["scrape"]["html"]["url"]." ";
      return;
    }
    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);
    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;

      $node["new_price"] = SH::extractPrice(HH::qVal($crawler, ".special-price .price"), "", ".");
      unset($crawler);
    }

    return $node;

  }
}
