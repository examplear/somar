<?hh

use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;

class se_Ginosnew extends BaseScraper {

  const Company_Id = 9108157;
  protected XML2ReaderGoogle $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2ReaderGoogle(parent::GetUrl(),"se_Ginosnew");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $offer = (array) $offer;

    if(isset($offer['g:id'])){
      $node = [];
      $node["format"] = "full_html";
      $node["company_id"] = self::Company_Id; //Constant from Drupal;


      $node["product_id"] = 'ginos-se-'.(string)$offer['g:id'].'-'.substr(md5((string)$offer['link']),0,10);
      $node["url"] = (string)$offer['link'];

      $node["title"] = utf8_decode(trim((string)$offer['title']));
      $node["raw_brand"] = utf8_decode((string)$offer['g:brand']);
      if(isset($offer['g:sale_price']) && !empty($offer['g:sale_price'])){
          $node["new_price"] = SH::extractPrice($offer['g:sale_price'],",");
      }
      $node["old_price"] = SH::extractPrice((string)$offer['g:price'],",");
      $node["image_small_url"] = str_replace('http://','https://',trim((string)$offer['g:image_link']));
      $node["description"] = utf8_decode(trim((string)$offer['title'])).' '.utf8_decode((string)$offer['description']);

      if(isset($offer['g:product_type']) && !empty((string) $offer['g:product_type'])){
        $cat =  str_replace('>','-',utf8_decode(html_entity_decode((string) $offer['g:product_type'])));

        $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim($cat),'UTF-8'));
        $c = json_decode(file_get_contents($catpath));

        $node["raw_categories"] = $cat;

        if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
          $ids =  explode(',', $c[0]->tax_group);
          foreach($ids as $id){
            $node["categories"][] = (int) $id;
          }
        }

      }

      return $node;
    }
    return NULL;
  }

  public function ExternalData(array &$node, array $html){

  }
}
