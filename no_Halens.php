<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class no_Halens extends BaseScraper {

  const Company_Id = 1943959;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "no");
    $this->reader = new XML2Reader(parent::GetUrl(),"no_Halens");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "NOK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "NO";
    $node["product_id"] = 'halens-no-'.(string)$offer->SKU.'-'.substr(md5((string)$offer->ProductUrl),0,5);
    $node["url"] = (string)$offer->TrackingUrl;
    $node["title"] = utf8_decode(trim((string)$offer->Name));
    $node["raw_brand"] = utf8_decode((string)$offer->Brand);
    $node["new_price"] = SH::extractPrice((string)$offer->Price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->OriginalPrice,",");
    $node["image_small_url"] = (string)$offer->ImageUrl;
    //$node["raw_colors"] = (string)$offer->Colour;
    //$node["raw_sizes"] = (string)$offer->size;
    $node["description"] = utf8_decode((string)$offer->Description);
    $node["summary"] = utf8_decode(SH::text_summary((string)$offer->Name));
    $node["raw_categories"] = utf8_decode(preg_replace('/(?:\s\s+|\n|\t)/', ' ',(string)$offer->Category));

    $nam =  (array) $offer->Extras->Name;
    $val =  (array) $offer->Extras->Value;

    if($nam[0] == 'GENDER'){
        $gen = $val[0];
    }
    else if($nam[1] == 'GENDER'){
        $gen = $val[1];
    }
    else if($nam[2] == 'GENDER'){
        $gen = $val[2];
    }
    else{
      $gen = '';
    }

   if($gen == "female"){
        $node["gender"] = 1;

      } elseif($gen == "male"){
        $node["gender"] = 0;
      }
      else {
        $node["gender"] = 2;
      }


    $nam1 =  (array) $offer->Extras->Name;
    $val1 =  (array) $offer->Extras->Value;

    if($nam1[0] == 'COLOUR'){
        $col1 = $val1[0];
    }
    else if($nam1[1] == 'COLOUR'){
        $col1 = $val1[1];
    }
    else if($nam1[2] == 'COLOUR'){
        $col1 = $val1[2];
    }
    else{
      $col1 = '';
    }

    $node["raw_colors"] = utf8_decode($col1);



      return $node;
    }

  public function ExternalData(array &$node, array $html){

  }

}
