<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class de_Yas extends BaseScraper {

  const Company_Id =  1231234;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_Yas");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }


    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DE";
    $node["product_id"] = 'yas-de-'.(string)$offer->ID.'-'.substr(md5((string)$offer->URL),0,5);
    $node["url"] = utf8_decode((string)$offer->URL);
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->properties->brand->value);
    $node["new_price"] = SH::extractPrice((string)$offer->properties->salePrice->value,",");
    $node["old_price"] = SH::extractPrice((string)$offer->price->amount,",");
    $node["image_small_url"] = (string)$offer->images->image;
    $node["description"] = utf8_decode((string)$offer->description);
/*
  if ((string)$offer->properties->gender->value =="female") {
      $node["gender"] = 1;
      $node["raw_categories"] = utf8_decode((string)$offer->properties->subcategories->value.' - '.'female');
    } else if ((string)$offer->properties->gender->value =="male") {
      $node["gender"] = 0;
      $node["raw_categories"] = utf8_decode((string)$offer->properties->subcategories->value.' - '.'male');

    }else if ((string)$offer->properties->gender->value =="unisex") {
      $node["gender"] = 2;
      $node["raw_categories"] = utf8_decode((string)$offer->properties->subcategories->value.' - '. 'unisex');
    }
    else {
      $node["gender"] = NULL;
      $node["raw_categories"] = utf8_decode((string)$offer->properties->subcategories->value);
    }
*/



if(isset($offer->properties->gender->value)){
  $offer->properties->subcategories->value = $offer->properties->subcategories->value.' - '.$offer->properties->gender->value;
}


  if(isset($offer->properties->subcategories->value) && !empty($offer->properties->subcategories->value)){
      
      $catpath = 'http://localhost/smartster-categorizer/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=de&name_hash='.md5(mb_convert_case(utf8_decode(trim($offer->properties->subcategories->value)), MB_CASE_LOWER, "UTF-8"));


     $c = json_decode(file_get_contents($catpath));

     $node["raw_categories"] = utf8_decode((string)$offer->properties->subcategories->value);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){             
             $node["categories"][] = (int) $id;
           }
      }


    }
    return $node;
  }


/*
 if(isset($offer->properties->subcategories->value) && !empty($offer->properties->subcategories->value)){
     echo $catpath = 'http://localhost/smartster-categorizer/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=de&name_hash='.md5(mb_strtolower(trim($node["raw_categories"]),'UTF-8'));

die();
     $c = json_decode(file_get_contents($catpath));

     //$node["raw_categories"] = utf8_decode((string)$offer->properties->subcategories->value);


          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

*/
  public function ExternalData(array &$node, array $html){

  }

}
