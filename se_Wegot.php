<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Monolog\Logger;

class se_Wegot extends BaseScraper {

  const Company_Id = 7565807;

  protected CSVReader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new CSVReader(parent::GetUrl(),"se_wegot","\t");
  }
  public function GetRandomIndex(): int {
      return rand(10000, 100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {

    // loop through the file line-by-line
    if(($data = $this->reader->Next()) === NULL){
      return NULL;
    }

    $offer = $data;
    // resort/rewrite data and insert into DB here
    // try to use conditions sparingly here, as those will cause slow-performance
    if($offer && isset($offer[0])){

      $node = [];

      $node["new_price"] = SH::extractPrice((string)$offer[7],",");
      $node["old_price"] = SH::extractPrice((string)$offer[6],",");
      $node["format"] = "full_html";
      $node["company_id"] = self::Company_Id; //Constant from Drupal;
      $node["product_id"] = 'wegot-'.md5((string)$offer[0]);
      $node["title"] = ((string)$offer[1]);
      $node["description"] = trim((string)$offer[2]);
      $node["summary"] = SH::text_summary(trim((string)$offer[2]));
      $node["raw_categories"] = (string)$offer[14];
      $node["image_small_url"] = (string)$offer[4];
      $node["image_small_url"] = str_replace('https','http',$node["image_small_url"]);
      $node["url"] = (string)$offer[3];
      $node["url"] = str_replace('google_shopping','smartster',$node["url"]);
      $node["raw_brand"] = (string)$offer[11];

      unset($data);
      return $node;
    }
    return NULL;
  }
  public function ExternalData(array &$node, array $scraped){
  }
}
