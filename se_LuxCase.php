<?hh

use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;

class se_LuxCase extends BaseScraper {
const Company_Id = 8185114;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_LuxCase");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'luxcase-'.(string)$offer->ID;
    $node["title"] = html_entity_decode(utf8_decode(trim((string)$offer->name)));
    $node["url"] = (string)$offer->URL;
    $node["image_small_url"] = (string)$offer->images->image;
    $node["description"] = htmlentities(utf8_decode((string)$offer->description), ENT_QUOTES, "UTF-8");
    $node["new_price"] = SH::extractPrice((string)$offer->price->amount,",");
    $node["old_price"] = SH::extractPrice((string)$offer->properties->fromPrice->value,",");
    $node["raw_brand"] = (string)$offer->properties->phone_brand->value;
    $node["raw_categories"] = utf8_decode((string)$offer->properties->category->value);

    return $node;
  }


  public function ExternalData(array &$node, array $html){

  }
}
