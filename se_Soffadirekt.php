<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Monolog\Logger;

class se_Soffadirekt extends BaseScraper {

  const Company_Id = 7509067;

  protected CSVReader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new CSVReader(parent::GetUrl(),"se_soffadirekt","\t");
  }
  public function GetRandomIndex(): int {
      return rand(1, 100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {

    // loop through the file line-by-line
    if(($data = $this->reader->Next()) === NULL){
      return NULL;
    }

    $offer = $data;
    // resort/rewrite data and insert into DB here
    // try to use conditions sparingly here, as those will cause slow-performance
    if($offer && isset($offer[1])){
      
      $node = [];

      $node["new_price"] = SH::extractPrice((string)$offer[2],",");
      $node["format"] = "full_html";
      $node["company_id"] = self::Company_Id; //Constant from Drupal;
      $node["product_id"] = 'soffadirekt-'.md5((string)$offer[1]);
      $node["title"] = utf8_encode((string)$offer[4]);
      $node["description"] = utf8_encode(trim((string)$offer[9]));
      $node["summary"] = SH::text_summary(utf8_encode(trim((string)$offer[4])));
      $node["raw_categories"] = utf8_encode((string)$offer[0]);
      $node["image_small_url"] = utf8_encode((string)$offer[10]);
      $node["url"] = (string)$offer[3];
      $node["raw_brand"] = utf8_encode((string)$offer[6]);
      $node["scrape"] = ["html" => ["url" => (string)$offer[3], "method" => "GET"]];

      unset($data);
      return $node;
    }
    return NULL;
  }
  public function ExternalData(array &$node, array $scraped){
    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"].":".$node["scrape"]["html"]["url"]." ";
      return;
    }
    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);
    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;
      
      $node["old_price"] = SH::extractPrice(HH::qVal($crawler, "#PrisRek"), "", ".");      
      
      unset($crawler);
    }

    return $node;
  }
}
