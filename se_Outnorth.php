<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use Underscore\Underscore as _;
use Monolog\Logger;

class se_Outnorth extends BaseScraper {

  const Company_Id = 622266;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_outnorth");
  }
  public function GetRandomIndex(): int {
      return rand(1000, 100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next();
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;

    $node["product_id"] = 'outnorth-'.(string)$offer->SKU;
    $node["url"] = (string)$offer->TrackingUrl;
    $node["title"] = (string)$offer->Name;
    $node["new_price"] = (string)$offer->Price;
    $node["image_small_url"] = (string)$offer->ImageUrl;
    $node["description"] = (string)$offer->Description;
    $node["summary"] = SH::text_summary((string)$offer->Description);
    $node["raw_brand"] = (string)$offer->brand;

    if(mb_strlen($node["summary"]) > 500){
      var_dump($node["summary"]);
    }
    $node["raw_categories"] = (string)$offer->Category;

    $node["raw_sizes"] = [];
    $node["raw_colors"] = [];

    $node["gender"] = NULL;



    return $node;
  }
  public function ExternalData(array &$node, array $scraped){
    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"]." ";
      return;
    }
    $crawler = HH::XpathFromHtml($scraped["html"]["content"]);

    $price = HH::querySelector($crawler, "#nosto-prod-wrapper .list_price");
    if($price === NULL){
      $price = "";
    }else {
      $price = $price->nodeValue;
    }
    $new_value = SH::extractPrice($price,".");

    $node["old_price"] = $new_value;

    unset($crawler);

  }
}
