<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class de_ChristineLeDuc extends BaseScraper {

  const Company_Id = 8884932;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_ChristineLeDuc");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DE";
    $node["product_id"] = 'christineLeDuc-de-'.(string)$offer->ID;
    $node["url"] = (string)$offer->URL;
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->properties->brand->value);
    $node["new_price"] = (string)$offer->price->amount;
    $node["image_small_url"] = (string)$offer->images->image;
    $node["description"] = utf8_decode((string)$offer->description);
    $node["raw_colors"] = explode(",",(string)$offer->properties->color->value);    
    $node["raw_sizes"] = explode(",",(string)$offer->properties->size->value);
    $node["summary"] = utf8_decode(SH::text_summary((string)$offer->name));

    $c_array = array(
      'Love Toys > BDSM-Produkte > BDSM Peitschen & Paddles',
      'Love Toys > BDSM-Produkte > BDSM Toys > Augenmasken',
      'Love Toys > BDSM-Produkte > BDSM Toys > BDSM Halsbänder',
      'Love Toys > BDSM-Produkte > BDSM Toys > Mundknebel',
      'Love Toys > BDSM-Produkte > Bondage-Sets',
      'Love Toys > BDSM-Produkte > Elektrosex',
      'Love Toys > BDSM-Produkte > Handschellen & Fesseln',
      'Love Toys > BDSM-Produkte > Love Toys',
      'Love Toys > BDSM-Produkte > Nippelklemmen',
      'Love Toys > BDSM-Produkte > SM-Möbel',
      'Love Toys > DVDs > DVDs für IHN > 3 DVDs 15 Euro',
      'Love Toys > DVDs > DVDs für IHN > Afrikanerinnen',
      'Love Toys > DVDs > DVDs für IHN > Amateure',
      'Love Toys > DVDs > DVDs für IHN > Anal-Sex',
      'Love Toys > DVDs > DVDs für IHN > Asiatinnen',
      'Love Toys > DVDs > DVDs für IHN > Bi-Sex-DVDs',
      'Love Toys > DVDs > DVDs für IHN > Bondage',
      'Love Toys > DVDs > DVDs für IHN > DVD-Bestseller',
      'Love Toys > DVDs > DVDs für IHN > DVD-Neuerscheinungen',
      'Love Toys > DVDs > DVDs für IHN > DVD-Pakete',
      'Love Toys > DVDs > DVDs für IHN > DVDs, Frankreich',
      'Love Toys > DVDs > DVDs für IHN > Dicke, BBW',
      'Love Toys > DVDs > DVDs für IHN > Doppelpenetration',
      'Love Toys > DVDs > DVDs für IHN > Erotikstars',
      'Love Toys > DVDs > DVDs für IHN > Extrem',
      'Love Toys > DVDs > DVDs für IHN > Fetisch-Erotik',
      'Love Toys > DVDs > DVDs für IHN > Fisting',
      'Love Toys > DVDs > DVDs für IHN > Frauenfreundlich',
      'Love Toys > DVDs > DVDs für IHN > Gay-DVDs',
      'Love Toys > DVDs > DVDs für IHN > Gruppensex',
      'Love Toys > DVDs > DVDs für IHN > Hetero, diverse',
      'Love Toys > DVDs > DVDs für IHN > Jung & Alt',
      'Love Toys > DVDs > DVDs für IHN > Lesben-DVDs',
      'Love Toys > DVDs > DVDs für IHN > MILFs',
      'Love Toys > DVDs > DVDs für IHN > Masturbation',
      'Love Toys > DVDs > DVDs für IHN > Natursekt, Pissing',
      'Love Toys > DVDs > DVDs für IHN > Oldies',
      'Love Toys > DVDs > DVDs für IHN > SM',
      'Love Toys > DVDs > DVDs für IHN > Squirting',
      'Love Toys > DVDs > DVDs für IHN > Stark behaart',
      'Love Toys > DVDs > DVDs für IHN > Teenager, 18+',
      'Love Toys > DVDs > DVDs für IHN > Transen & She-males',
      'Love Toys > DVDs > DVDs für IHN > Voyeure',
      'Love Toys > DVDs > DVDs für IHN > XXX-Spielfilme',
      'Love Toys > Love Toys für Frauen > Anal-Spielzeug > Anal-Dildos',
      'Love Toys > Love Toys für Frauen > Anal-Spielzeug > Anal-Vibratoren',
      'Love Toys > Love Toys für Frauen > Anal-Spielzeug > Analketten',
      'Love Toys > Love Toys für Frauen > Dildos > Dildos mit Saugnapf',
      'Love Toys > Love Toys für Frauen > Dildos > Doppeldildos',
      'Love Toys > Love Toys für Frauen > Dildos > Glasdildos',
      'Love Toys > Love Toys für Frauen > Dildos > Natur-Dildos',
      'Love Toys > Love Toys für Frauen > Dildos > Umschnalldildos',
      'Love Toys > Love Toys für Frauen > Liebeskugeln',
      'Love Toys > Love Toys für Frauen > Neuheiten',
      'Love Toys > Love Toys für Frauen > Stimulatoren',
      'Love Toys > Love Toys für Frauen > Stimulatoren > Fingervibratoren',
      'Love Toys > Love Toys für Frauen > Stimulatoren > Klitoris-Stimulatoren',
      'Love Toys > Love Toys für Frauen > Stimulatoren > Strap-on-Stimulatoren',
      'Love Toys > Love Toys für Frauen > Stimulatoren > Vagina-Pumpen',
      'Love Toys > Love Toys für Frauen > Stimulatoren > Vibrator-Slips',
      'Love Toys > Love Toys für Frauen > Vibratoren > Anal-Vibratoren',
      'Love Toys > Love Toys für Frauen > Vibratoren > Designer-Vibratoren',
      'Love Toys > Love Toys für Frauen > Vibratoren > G-Punkt-Vibratoren',
      'Love Toys > Love Toys für Frauen > Vibratoren > Heiße Preise für Anfängertoys',
      'Love Toys > Love Toys für Frauen > Vibratoren > Klitoris-Vibratoren',
      'Love Toys > Love Toys für Frauen > Vibratoren > Mini-Vibratoren',
      'Love Toys > Love Toys für Frauen > Vibratoren > Natur-Vibratoren',
      'Love Toys > Love Toys für Frauen > Vibratoren > Rabbit-Vibratoren',
      'Love Toys > Love Toys für Frauen > Vibratoren > Stab-Vibratoren',
      'Love Toys > Love Toys für Frauen > Vibratoren > Vibratoren mit Fernbedienung',
      'Love Toys > Love Toys für Frauen > Vibratoren > XL-Vibratoren',
      'Love Toys > Love Toys für Frauen > Vibro-Eier',
      'Love Toys > Love Toys für Männer > Anal-Spielzeug > Anal-Dildos',
      'Love Toys > Love Toys für Männer > Anal-Spielzeug > Anal-Ketten',
      'Love Toys > Love Toys für Männer > Anal-Spielzeug > Anal-Vibratoren',
      'Love Toys > Love Toys für Männer > Anal-Spielzeug > Analplugs',
      'Love Toys > Love Toys für Männer > Anal-Spielzeug > Prostata-Vibratoren',
      'Love Toys > Love Toys für Männer > Masturbatoren',
      'Love Toys > Love Toys für Männer > Penis-Extender',
      'Love Toys > Love Toys für Männer > Penis-Plugs',
      'Love Toys > Love Toys für Männer > Penishüllen',
      'Love Toys > Love Toys für Männer > Penispumpen',
      'Love Toys > Love Toys für Männer > Penisringe',
      'Love Toys > Love Toys für Männer > Sexpuppen',
      'Love Toys > Love Toys für Paare > Love Toy- & Gift-Sets',
      'Love Toys > Love Toys für Paare > Partnertoys',
      'Love Toys > Love Toys für Paare > Penisringe',
      'Love Toys > Love Toys für Paare > Strapons > Strap-on-Dildos',
      'Love Toys > Sexspielzeug für IHN > Penisringe',
      'Outlet > Love Toys',
      'Outlet > Love Toys > Anale Love Toys',
      'Outlet > Love Toys > BDSM',
      'Outlet > Love Toys > Vibrator-Eier',
      'Outlet > Love Toys > Vibratoren > Rabbit-Vibratoren',
      'Drogerie > Für IHN > Anal-Pflege > Anal-Duschen',
      'Drogerie > Für IHN > Spanische Fliege',
      'Drogerie > Für Paare > Spanische Fliege',
      'Drogerie > Für SIE > Klitoris-Stimulation',
      'Drogerie > Für SIE > Anal-Pflege > Anal-Duschen',
      'Drogerie > Für SIE > Anal-Pflege > Anal-Relax',
      'Drogerie > Für SIE > Anal-Pflege > Bleaching',
      'Drogerie > Für SIE > Aphrodisiaka',
      'Drogerie > Für SIE > Spanische Fliege',
      'Wohnen & Lifestyle > Gadgets > Aufregende Gadgets',
      'Wohnen & Lifestyle > Gadgets > Aufregende Gadgets > Essbar',
      'Wohnen & Lifestyle > Gadgets > Aufregende Gadgets > Fun',
      'Wohnen & Lifestyle > Gadgets > Junggesellenabschied',
      'Outlet > Mode'
    );

  if(!in_array(utf8_decode((string)$offer->properties->categoryPath->value),$c_array)){
    $node["old_price"] = (string)$offer->properties->oldPrice->value;
  }
  if ((string)$offer->properties->gender->value =="Damen") {
      $node["gender"] = 1;

    } else if ((string)$offer->properties->gender->value =="Herren") {
      $node["gender"] = 0;

    }else if ((string)$offer->properties->gender->value =="Unisex") {
      $node["gender"] = 2;
    }
    else {
      $node["gender"] = NULL;
    }

if(isset($offer->properties->categoryPath->value) && !empty($offer->properties->categoryPath->value)){
      $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=de&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->properties->categoryPath->value)),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

     $node["raw_categories"] = utf8_decode((string)$offer->properties->categoryPath->value);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
