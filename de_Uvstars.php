<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class de_Uvstars extends BaseScraper {

  const Company_Id = 8350977;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_Uvstars");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }


    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DE";
    $node["product_id"] = 'uvstars-de-'.(string)$offer->ID.'-'.substr(md5((string)$offer->URL),0,5);
    $node["url"] = utf8_decode((string)$offer->URL);
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->properties->gender->value);
    $node["new_price"] = SH::extractPrice((string)$offer->price->amount,",");
    $node["old_price"] = SH::extractPrice((string)$offer->properties->old_price->value,",");
    $node["image_small_url"] = (string)$offer->images->image;

   // $node["raw_sizes"] = [(string)$offer->properties->size->value];
    //$node["raw_colors"] = explode(",",(string)$offer->properties->color->value);

    $colors = preg_replace('!\s+!', ' ', str_replace('',' ',str_replace('Â',' ',$offer->properties->color->value)));    
    $node["raw_colors"] =     utf8_decode(mb_convert_encoding($colors,"UTF-8"));           

    $sizes = preg_replace('!\s+!', ' ', str_replace('',' ',str_replace('Â',' ',$offer->properties->size->value)));    
    $node["raw_sizes"] =     utf8_decode(mb_convert_encoding($sizes,"UTF-8"));           

    $node["description"] = utf8_decode((string)$offer->description);    
    $node["summary"] = utf8_decode(SH::text_summary((string)$offer->properties->descriptionLong->value));

    if ((string)$offer->properties->EAN->value =="Damen") {
      $node["gender"] = 1;

    } else if ((string)$offer->properties->EAN->value =="Herren") {
      $node["gender"] = 0;

    }else if ((string)$offer->properties->EAN->value =="Damen,Herren") {
      $node["gender"] = 2;
    }
    else {
      $node["gender"] = NULL;
    }


  if(isset($offer->properties->categoryPath->value) && !empty($offer->properties->categoryPath->value)){
      $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=de&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->properties->categoryPath->value)),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

     $node["raw_categories"] = utf8_decode((string)$offer->properties->categoryPath->value);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
