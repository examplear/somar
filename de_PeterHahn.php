<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class de_PeterHahn extends BaseScraper {

  const Company_Id = 7756411;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_PeterHahn");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DE";
    //$node["product_id"] = 'peterhahn-de-'.(string)$offer->ID;
    $node["product_id"] = 'peterhahn-de-'.(string)$offer->ID.'-'.md5((string)$offer->URL);
    $node["url"] = (string)$offer->URL;
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["image_small_url"] = (string)$offer->images->image;
    $node["description"] = utf8_decode((string)$offer->description);
    $node["summary"] = utf8_decode(SH::text_summary((string)$offer->name));
    $node["new_price"] = SH::extractPrice((string)$offer->properties->lOWEST_PRICE->value,",");
    $node["old_price"] = SH::extractPrice((string)$offer->properties->dISCOUNTED_PRICE->value,",");

    $node["raw_brand"] = utf8_decode((string)$offer->properties->brand->value);
    $node["raw_sizes"] = explode(",",(string)$offer->properties->size->value);
    //$node["raw_colors"] = [utf8_decode((string)$offer->properties->color->value)];
    $node["raw_colors"] =   (string)$offer->properties->color->value;
    $node["raw_categories"] = utf8_decode((string)$offer->categories->category);

    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
