<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class se_Skapamer extends BaseScraper {

  const Company_Id = 5033389;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Skapamer");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'skapamer-se-'.(string)$offer->SKU.'-'.substr(md5((string)$offer->productUrl),0,5);
    $node["url"] = (string)$offer->productUrl;
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->regularPrice,",");
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["image_small_url"] = (string)$offer->graphicUrl;
    $node["raw_brand"] = utf8_decode((string)$offer->brand);

   $des =  (string) $offer->description;

   if(!empty($des)){   
     $node["description"] =  utf8_decode($des);
   }
   else
   {
    $node["description"]  =  utf8_decode((string) $offer->name);
   }

    $cat = '';
    if($offer->categories){
      
      $a=0;
      foreach($offer->categories->category as $categor)
      {                         
        if($a!=0)
        {               
          $cat .=' '.$categor;  
        }else
        {             
          $cat .=$categor;  
        }
        $a++;
      }    
     $node["raw_categories"] =  utf8_decode($cat);                 
    }

    if(isset($cat) && !empty($cat)){
        $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim($node["raw_categories"]),'UTF-8'));
        $c = json_decode(file_get_contents($catpath));

        //$node["raw_categories"] = utf8_decode((string)$offer->Category);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
         $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }

    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
