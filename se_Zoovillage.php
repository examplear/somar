<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class se_Zoovillage extends BaseScraper {

  const Company_Id = 10215508;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Zoovillage");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'zoovillage-se-'.(string)$offer->SKU.'-'.substr(md5((string)$offer->TrackingUrl),0,5);
    $node["url"] = (string)$offer->TrackingUrl;
    $node["new_price"] = SH::extractPrice((string)$offer->Price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->OriginalPrice,",");
    $node["title"] = utf8_decode(trim((string)$offer->Name));
    $node["image_small_url"] = (string)$offer->ImageUrl;
    $node["raw_brand"] = utf8_decode((string)$offer->Brand);
 
   $des = utf8_decode((string)$offer->Description);
   if(!empty($des)){
      $node["description"] = $des;
   }
   else
   {
      $node["description"]  =  utf8_decode(trim((string)$offer->Name));
   }

  if(isset($offer->Category) && !empty($offer->Category)){

  $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->Category)),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

     $node["raw_categories"] = utf8_decode((string)$offer->Category);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}

