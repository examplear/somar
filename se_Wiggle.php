<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class se_Wiggle extends BaseScraper {

  const Company_Id = 8379392;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Wiggle");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'wiggle-'.(string)$offer['zupid'];
    $node["url"] = (string)$offer->deepLink;
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->manufacturer);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->oldPrice,",");
    $node["raw_colors"] = (string)$offer->color;
    $node["raw_sizes"] = [(string)$offer->size];
    $node["image_small_url"] = (string)$offer->smallImage;
    $node["image_large_url"] = (string)$offer->largeImage;
    $node["description"] = utf8_decode((string)$offer->description);
    $node["summary"] = SH::text_summary((string)$offer->longDescription);
    $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath);

    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }
}
