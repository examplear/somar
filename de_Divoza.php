<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class de_Divoza extends BaseScraper {

  const Company_Id = 8421436;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_Divoza");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }


    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DE";
    $node["product_id"] = 'divoza-de-'.(string)$offer->ID.'-'.substr(md5((string)$offer->URL),0,5);
    $node["url"] = utf8_decode((string)$offer->URL);
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->properties->brand->value);
    $node["new_price"] = SH::extractPrice((string)$offer->price->amount,",");
    $node["old_price"] = SH::extractPrice((string)$offer->properties->fromPrice->value,",");
    $node["image_small_url"] = (string)$offer->images->image;
    $node["description"] = utf8_decode((string)$offer->description);

   $cat = $offer->categories->category;
   $subcat = $offer->properties->subcategories->value;
   $subsubcat = $offer->properties->subsubcategories->value;

   if(!empty($subsubcat)){

     $node["raw_categories"]  = utf8_decode(trim((string)$cat.' - '.(string)$subcat.' - '.(string)$subsubcat));
   }
   else if(!empty($subcat))
   {
       $node["raw_categories"]  = utf8_decode(trim((string)$cat.' - '.(string)$subcat));
   }
   else
   {
      $node["raw_categories"]  = utf8_decode(trim((string)$cat));
   }


     
  //  $var = utf8_decode(trim((string)$offer->categories->category.' - '.$offer->properties->subcategories->value.' - '.$offer->properties->subsubcategories->value));
   // $node["raw_categories"] =  $var;


  if(isset($offer->categories->category) && !empty($offer->categories->category)){
      $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=de&name_hash='.md5(mb_strtolower(trim($node["raw_categories"]),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
