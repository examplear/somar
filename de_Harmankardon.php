<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
header('Content-Type: text/html; charset=utf-8');
class de_Harmankardon extends BaseScraper {

  const Company_Id = 7757753;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_Harmankardon");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'harmankardon-de-'.(string)$offer->ID.'-'.md5((string)$offer->URL);
    $node["country"] = "DE";
    $node["url"] = (string)$offer->URL;
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->properties->brand->value);
    $node["new_price"] = SH::extractPrice((string)$offer->price->amount,",");
    $node["old_price"] = SH::extractPrice((string)$offer->properties->product_list_price->value,",");
    $node["image_small_url"] = (string)$offer->images->image;
    $node["raw_colors"] = [utf8_decode((string)$offer->properties->color->value)];

   // $node["raw_sizes"] = [(string)$offer->properties->product_spec_value1->value];
    $node["description"] = utf8_decode((string)$offer->properties->descriptionLong->value);
    $node["summary"] = utf8_decode(SH::text_summary((string)$offer->properties->product_short_descriptor->value));
    $node["raw_categories"] = utf8_decode(trim((string)$offer->categories->category));



  return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
