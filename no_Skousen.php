<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class no_Skousen extends BaseScraper {

  const Company_Id = 5583530;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "no");
    $this->reader = new XML2Reader(parent::GetUrl(),"no_Skousen");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "NOK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "NO";
    $node["product_id"] = 'skousen-no-'.md5((string)$offer->SKU.(string)$offer->TrackingUrl);
    $node["url"] = (string)$offer->TrackingUrl;
    $node["title"] = utf8_decode(trim((string)$offer->Name));
    $node["old_price"] = SH::extractPrice((string)$offer->Price,",");
    $node["image_small_url"] = (string)$offer->ImageUrl;    
    $node["raw_brand"] = utf8_decode((string)$offer->Brand);

   $des =  (string) $offer->Description;

   if(!empty($des)){   
     $node["description"] =  utf8_decode($des);
   }
   else
   {
    $node["description"]  =  utf8_decode((string) $offer->Name);
   }


  $catvalue = str_replace('  ', ' ', $offer->Category);

  if(isset($offer->Category) && !empty($offer->Category)){

  $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=no&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$catvalue)),'UTF-8'));
    
    $c = json_decode(file_get_contents($catpath));

     $node["raw_categories"] = utf8_decode((string)$catvalue);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }

    $node["scrape"] = ["html" => ["url" => (string)$offer->ProductUrl, "method" => "GET"]];
    return $node;
  }

  public function ExternalData(array &$node, array $scraped){


    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"].":".$node["scrape"]["html"]["url"]." ";
      return;
    }
    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);
    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;

      $node["new_price"] = SH::extractPrice(HH::qVal($crawler, ".discount-module .discount-text .discount-price"), ",", ".");

      unset($crawler);
    }

    return $node;

  }
}

