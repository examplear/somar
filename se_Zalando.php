<?hh

use \Helpers\StringHelpers\StringHelpers as SH;
use Underscore\Underscore as _;
use Monolog\Logger;

class se_Zalando extends BaseScraper {

  const Company_Id = 33;

  protected CSVReader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new CSVReader(parent::GetUrl(),"se_zalando","\t");
  }
  public function GetRandomIndex(): int {
      return rand(40000, 100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {

    // loop through the file line-by-line
    if(($data = $this->reader->Next()) === NULL){
      return NULL;
    }

    $offer = $data;
    // resort/rewrite data and insert into DB here
    // try to use conditions sparingly here, as those will cause slow-performance
    if($offer && isset($offer[0])){
      $node = [];
      $node["format"] = "full_html";
      $node["company_id"] = self::Company_Id; //Constant from Drupal;

      $node["product_id"] = 'zalando-'.(string)$offer[0];
      $node["title"] = (string)$offer[1];
      $node["new_price"] = SH::extractPrice((string)$offer[2],",");
      $node["old_price"] = SH::extractPrice((string)$offer[3],",");

      $node["description"] = (string)$offer[4];
      $node["summary"] = SH::text_summary((string)$offer[4]);
      //$node["raw_categories"] = $offer[6];

      if(isset($offer[6]) && !empty($offer[6])){
          $catpath = 'http://beta.smartster.no/driver.php?table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(((string)$offer[6])),'UTF-8'));

          $c = json_decode(file_get_contents($catpath));
          $node["raw_categories"] = (string)$offer[6];
          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
             $ids =  explode(',', $c[0]->tax_group);
               foreach($ids as $id){
                 $node["categories"][] = (int) $id;
               }
          }
      }

      $node["image_small_url"] = (string)$offer[7];
      $node["image_large_url"] = (string)$offer[8];
      $node["url"] = (string)$offer[9];
      $node["raw_brand"] = (string)$offer[10];
      $node["raw_colors"] = [(string)$offer[11]];
      $node["raw_sizes"] = explode("|",(string)$offer[12]);

      $node["gender"] = $offer[13] == "Dam" ? 1 : ($offer[13] == "Herr" ? 0 : NULL);

      unset($data);
      return $node;
    }
    return NULL;
  }
  public function ExternalData(array &$node, array $html){

  }
}
