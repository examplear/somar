<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class de_Dresslily extends BaseScraper {

  const Company_Id = 8950978;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_Dresslily");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }


    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DE";
    $node["product_id"] = 'dresslily-de-'.(string)$offer->TDProductId.'-'.substr(md5((string)$offer->productUrl),0,5);
    $node["url"] = utf8_decode((string)$offer->productUrl);
    $node["title"] = htmlspecialchars_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->brand);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->previousPrice,",");
    $node["image_small_url"] = (string)$offer->imageUrl;
    $node["raw_sizes"] = [utf8_decode((string)$offer->size)];
    $node["description"] = htmlspecialchars_decode((string)$offer->description);
   
    $val = utf8_decode((string)$offer->merchantCategoryName);

    if (strpos($val,'Women') !== false) {
       $node["gender"] = 0;
    }
    else if (strpos($val,'Men') !== false) {
       $node["gender"] = 1;    
    }
    else if (strpos($val,'Unisex') !== false) {
       $node["gender"] = 2;  
    }else{
       $node["gender"] = null;      
    }


  if(isset($offer->merchantCategoryName) && !empty($offer->merchantCategoryName)){
      $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=de&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->merchantCategoryName)),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

     $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryName);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
