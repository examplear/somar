<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;

class se_Q4scandesign extends BaseScraper {

  const Company_Id = 6850696;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_q4scandesign");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;

    $node["product_id"] = 'q4scandesign-'.(string)$offer->tilbudId;
    $node["url"] = (string)$offer->ehandelLink;
    $node["title"] = trim((string)$offer->tilbudNavn);

    $node["new_price"] = SH::extractPrice((string)$offer->tilbudPrisNu,",");
    $node["old_price"] = SH::extractPrice((string)$offer->tilbudPrisFoer,",");
    $node["accept"]  = True; // Only set if you want all products
    $node["image_small_url"] = (string)$offer->billedeURL;
    $node["description"] = (string)$offer->tilbudBeskrivelse;
    $node["summary"] = SH::text_summary((string)$offer->tilbudBeskrivelse); // TODO

    $node["raw_categories"] = (string)$offer->kategorier->kategori->kategoriNavn;


    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }


}
