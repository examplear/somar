<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class no_Nike extends BaseScraper {

  const Company_Id = 3786941;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "no");
    $this->reader = new XML2Reader(parent::GetUrl(),"no_Nike");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "NOK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "NO";
    $node["product_id"] = 'nike-no-'.(string)$offer->product_id;
    $node["url"] = (string)$offer->deeplink;
    $node["title"] = utf8_decode(trim((string)$offer->product_name));
    $node["raw_brand"] = utf8_decode((string)$offer->brand);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->Full_merchant_price,",");
    $node["image_small_url"] = (string)$offer->image_url;
    $node["raw_colors"] = (string)$offer->Colour;
    $node["raw_sizes"] = (string)$offer->size;
    $node["description"] = utf8_decode((string)$offer->description);
    $node["summary"] = utf8_decode(SH::text_summary((string)$offer->product_name));

    if($offer->gender == "Female"){
      $node["gender"] = 1;
    } elseif($offer->gender == "Male"){
      $node["gender"] = 0;
    }
    else {
      $node["gender"] = NULL;
    }

    $node["raw_categories"] = (string)$offer->merchant_category;

    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
