<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class no_Eyerim extends BaseScraper {

  const Company_Id = 3544318;
  protected CSVReader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "no");
    $this->reader = new CSVReader(parent::GetUrl(),"no_Eyerim","\t");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "NOK";
  }

  public function ScrapeNext() : ?array {

    // loop through the file line-by-line
    if(($data = $this->reader->Next()) === NULL){
      return NULL;
    }

    $offer = $data;
    if($offer && isset($offer[3])){
      
       $node = [];
       $node["format"] = "full_html";
       $node["company_id"] = self::Company_Id; //Constant from Drupal;
       $node["country"] = "NO";
       $node["product_id"] = 'eyerim-no-'.utf8_decode(str_replace('"', "", ((string)$offer[0]))).'-'.md5(utf8_decode(str_replace('"', "", ((string)$offer[5]))));
       $node["title"] =   utf8_decode(str_replace('"', "", ((string)$offer[1])));
       $node["description"] =   htmlspecialchars_decode(str_replace('"', "", ((string)$offer[2])));
       $node["image_small_url"] =  str_replace('"', "", ((string)$offer[23]));
       $node["url"] = str_replace('"', "", ((string)$offer[5]));
       $node["raw_brand"] = str_replace('"', "", ((string)$offer[12]));
       $node["new_price"] = SH::extractPrice(str_replace('"', "", ((string)$offer[9])),".");
       $node["old_price"] = SH::extractPrice(str_replace('"', "", ((string)$offer[8])),".");
       $node["raw_sizes"] = [utf8_decode(str_replace('"', "", ((string)$offer[19])))];
       $node["raw_colors"] = [utf8_decode(str_replace('"', "", ((string)$offer[16])))];

     $cat = utf8_decode(str_replace('"', "", ((string)$offer[3])));

    if (str_replace('"', "", ((string)$offer[15])) =="female") {
      $node["gender"] = 1;
      $node["raw_categories"] = $cat.' - female';

    } else if (str_replace('"', "", ((string)$offer[15])) =="male") {
      $node["gender"] = 0;
      $node["raw_categories"] = $cat.' - male';

    }else if (str_replace('"', "", ((string)$offer[15])) =="unisex") {
      $node["gender"] = 2;
      $node["raw_categories"] = $cat.' - unisex';
    }
    else {
      $node["gender"] = NULL;
      $node["raw_categories"] = $cat;
    }
    
     if(isset($cat) && !empty($cat)){
       
       $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=no&name_hash='.md5(mb_strtolower(trim($node["raw_categories"]),'UTF-8'));
     
       $c = json_decode(file_get_contents($catpath));
    
       //  $node["raw_categories"] =  htmlspecialchars_decode(str_replace('"', "", ((string)$offer[3])));

            if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
             $ids =  explode(',', $c[0]->tax_group);
             foreach($ids as $id){
               $node["categories"][] = (int) $id;
             }
        }

      }
      return $node;
    }
    return NULL;
  }

  public function ExternalData(array &$node, array $html){

  }

}






































 

