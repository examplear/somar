<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
header('Content-Type: text/html; charset=utf-8');
class se_Boozt extends BaseScraper {

  const Company_Id = 1041386;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Boozt");
  }
  public function GetRandomIndex(): int {
      return rand(30000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'boozt-se-'.(string)$offer->fields->product_id.'-'.substr(md5((string)$offer->productUrl),0,5);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->fields->normal_price,",");
    $node["title"] = htmlspecialchars_decode(trim((string)$offer->name));
    $node["raw_brand"] = htmlspecialchars_decode((string)$offer->brand);
    $node["image_small_url"] = (string)$offer->imageUrl;
    $node["url"] = (string)$offer->productUrl;
    $node["raw_sizes"] =  array_slice(explode(",",(string)$offer->fields->size),0,6);    
    $node["raw_colors"] = array_slice(explode("#",(string)$offer->fields->colours),0,6);    

   // $node["raw_colors"] = array_slice(explode("/",(string)$offer->fields->colours),0,6);    
   
   $des = htmlspecialchars_decode((string)$offer->description);
   if(!empty($des)){
      $node["description"] = $des;
   }
   else
   {
      $node["description"]  =  htmlspecialchars_decode((string)$offer->name);
   }

    if ((string)$offer->fields->gender =="women") {
      $node["gender"] = 1;

    } else if ((string)$offer->fields->gender =="men") {
      $node["gender"] = 0;

    }
    else if ((string)$offer->fields->gender =="unisex") {
      $node["gender"] = 2;
    }
    else {
      $node["gender"] = NULL;
    }

    if(isset($offer->merchantCategoryName) && !empty($offer->merchantCategoryName)){
        $catpath = 'http://beta.smartster.no/driver.php?table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->merchantCategoryName)),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

     $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryName);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }

    return $node;
  }

   public function ExternalData(array &$node, array $html){

  }


}
