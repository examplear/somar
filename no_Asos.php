<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class no_Asos extends BaseScraper {

  const Company_Id = 1200280;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "no");
    $this->reader = new XML2Reader(parent::GetUrl(),"no_Asos");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "NOK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "NO";
    $node["product_id"] = 'asos-no-'.(string)$offer['zupid'].'-'.substr(md5((string)$offer->deepLink),0,5);
    $node["url"] = utf8_decode((string)$offer->deepLink);
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->manufacturer);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->oldPrice,",");
    $node["image_small_url"] = (string)$offer->mediumImage;
    $node["description"] = utf8_decode((string)$offer->description);
    $node["raw_sizes"] = explode(",",(string)$offer->size);
    $node["raw_colors"] = [utf8_decode((string)$offer->color)];

    $val = utf8_decode((string)$offer->merchantCategoryPath);

   if (strpos($val,'MALE') !== false) {
      $node["gender"] = 0;
   }
   else if (strpos($val,'FEMALE') !== false) {
      $node["gender"] = 1;
   }
   else if (strpos($val,'Unisex') !== false) {
      $node["gender"] = 2;
   }else{
      $node["gender"] = null;
   }


  if(isset($offer->merchantCategoryPath) && !empty($offer->merchantCategoryPath)){

  $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=no&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->merchantCategoryPath)),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

     $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
