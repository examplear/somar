<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class se_Apotek365 extends BaseScraper {

  const Company_Id = 8073750;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Apotek365");
  }
  public function GetRandomIndex(): int {
      return rand(70000,10000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'apotek365-'.md5((string)$offer->SKU.(string)$offer->productUrl);
    $node["url"] = (string)$offer->productUrl;
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->brand);
    $node["image_small_url"] = (string)$offer->graphicUrl;
    $node["description"] = utf8_decode((string)$offer->description);
    $node["raw_categories"] = utf8_decode(parent::Combine($offer->categories->category));


 $categories = array( "Knipkulor","Balls","Oral","Penis","Penisförlängare","Penispump","Pumps","Realistic Vibrators
","Vibrator","Vibrator G-Punkten","Vibrator Mini","Vibrator Rabbit","Vibrator Set", "Vibrator Stav
","Vibrator penisformad","Dildo Design","Dildo Penisformad","Dildo Sprutande","Dildo Stavar","Dildo Vibrerande","Dubbel dildo","Klitoris stimulator","Lusthöjande Oralt","Penisring","Penisring med Pungsele","Penisring med Vibrator","Vibrator med rabbit","Uppblåsbara sexmöbler","Utgångna produkter","Sets","G-Spot","Analdildo","Fingervibrator","Vibrator Design","Vibrator Penisformad","Vibrator Stav","Vibrator Stavar","Penispumpar","Vibrator penisformad");

 if(!in_array(utf8_decode(parent::Combine($offer->categories->category)),$categories)){

               $node["new_price"] = SH::extractPrice((string)$offer->price,",");
               $node["old_price"] = SH::extractPrice((string)$offer->regularPrice,",");
 }

    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }


}
