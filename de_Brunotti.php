<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class de_Brunotti extends BaseScraper {

  const Company_Id = 8643482;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_Brunotti");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DE";
    $node["product_id"] = 'brunotti-de-'.(string)$offer->ID.'-'.substr(md5((string)$offer->URL),0,5);
    $node["url"] = utf8_decode((string)$offer->URL);
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->properties->brand->value);
    $node["new_price"] = SH::extractPrice((string)$offer->price->amount,",");
    $node["old_price"] = SH::extractPrice((string)$offer->properties->fromPrice->value,",");
    $node["image_small_url"] = (string)$offer->images->image;
    
   $des = utf8_decode((string)$offer->description);
   if(!empty($des)){
      $node["description"] = $des;
   }
   else
   {
      $node["description"]  =  utf8_decode(trim((string)$offer->name));
   }

   if ((string)$offer->properties->gender->value =="Damen" || (string)$offer->properties->gender->value =="Mädchen") {
      $node["gender"] = 1;

    } else if ((string)$offer->properties->gender->value =="Herren" || (string)$offer->properties->gender->value =="Jungen") {
      $node["gender"] = 0;

    }else if ((string)$offer->properties->gender->value =="Unisex") {
      $node["gender"] = 2;
    }
    else {
      $node["gender"] = NULL;
    }

    $node["raw_sizes"] = explode("|",(string)$offer->properties->size->value);
    $node["raw_colors"] = [utf8_decode((string)$offer->properties->color->value)];

   $subcat = $offer->properties->subcategory->value;
   // $subsubcat = $offer->properties->subsubcategory->value;

      if(isset($offer->properties->subsubcategory->value) && !empty($offer->properties->subsubcategory->value)){
          if ((string)$offer->properties->gender->value =="Damen") {
             $node["raw_categories"] =  ('Damen - '. utf8_decode((string)$subcat.' - '.(string)$offer->properties->subsubcategory->value));
          }
         else if ((string)$offer->properties->gender->value =="Herren") {
              $node["raw_categories"] =  ('Herren - '. utf8_decode((string)$subcat.' - '.(string)$offer->properties->subsubcategory->value));
         }
         else if ((string)$offer->properties->gender->value =="Mädchen") {

              $node["raw_categories"] =  ('Mädchen - '. utf8_decode((string)$subcat.' - '.(string)$offer->properties->subsubcategory->value));
          }
         else if ((string)$offer->properties->gender->value =="Jungen") {
              $node["raw_categories"] =  ('Jungen - '. utf8_decode((string)$subcat.' - '.(string)$offer->properties->subsubcategory->value));
         }
         else if ((string)$offer->properties->gender->value =="Damen und Herren") {
              $node["raw_categories"] =  ('Damen und Herren - '. utf8_decode((string)$subcat.' - '.(string)$offer->properties->subsubcategory->value));
         }
         else
         {
           $node["raw_categories"] =  utf8_decode((string)$subcat.' - '.(string)$offer->properties->subsubcategory->value);
         }
       }
       else
       {     
         if ((string)$offer->properties->gender->value =="Damen") {

              $node["raw_categories"]  = ('Damen - '. utf8_decode((string)$subcat));     
          }
         else if ((string)$offer->properties->gender->value =="Herren") {
              $node["raw_categories"]  = ('Herren - '. utf8_decode((string)$subcat));     
         }
         else if ((string)$offer->properties->gender->value =="Mädchen") {

               $node["raw_categories"]  = ('Mädchen - '. utf8_decode((string)$subcat));     
          }
         else if ((string)$offer->properties->gender->value =="Jungen") {
              $node["raw_categories"]  = ('Jungen - '. utf8_decode((string)$subcat));     
         }
         else if ((string)$offer->properties->gender->value =="Damen und Herren") {
               $node["raw_categories"]  = ('Damen und Herren - '. utf8_decode((string)$subcat));     
         }
         else
         {
             $node["raw_categories"] =  utf8_decode(trim((string)$subcat));   
         }
       }

  if(isset($offer->properties->subcategory->value) && !empty($offer->properties->subcategory->value)){
      $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=de&name_hash='.md5(mb_strtolower(trim($node["raw_categories"]),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

    // $node["raw_categories"] = utf8_decode((string)$offer->merchantCategory);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
