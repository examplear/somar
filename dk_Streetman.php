<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class dk_Streetman extends BaseScraper {

  const Company_Id = 8500479;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "dk");
    $this->reader = new XML2Reader(parent::GetUrl(),"dk_Streetman");
  }
  public function GetRandomIndex(): int {
      return rand(70000,10000);
  }
  public function GetCurrency() : string {
    return "DKK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DK";
    $node["product_id"] = 'streetman-dk-'.(string)$offer->SKU.'-'.md5((string)$offer->url);
    $node["url"] = 'http://salestring.go2cloud.org/aff_c?offer_id=107&aff_id=1705&url='.(string)$offer->url.'?utm_medium=affiliate&utm_source=salestring&utm_campaign={affiliate_name}';
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->manufacturer);
    $node["new_price"] = SH::extractPrice((string)$offer->price,","); 
    $node["old_price"] = SH::extractPrice((string)$offer->guide_price,",");
    $node["raw_colors"] = [utf8_decode((string)$offer->color)];
    $node["raw_sizes"] = [(string)$offer->size];
    $node["image_small_url"] = (string)$offer->image;  
    $node["description"] = utf8_decode((string)$offer->description);
   
    if ((string)$offer->gender =="female") {
      $node["gender"] = 1;

    } else if ((string)$offer->gender =="male") {
      $node["gender"] = 0;

    }
    else if ((string)$offer->gender =="Unisex") {
      $node["gender"] = 2;
    }
    else {
      $node["gender"] = NULL;
    }

   
   if((string)$offer->gender){
     $g = ' - '.(string)$offer->gender;
   }else{
    $g = NULL; 
   }

    $cat = utf8_decode((string)$offer->category).' - '.utf8_decode((string)$offer->subcategory).$g;

    if(isset($cat) && !empty($cat)){     
     $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=dk&name_hash='.md5(mb_strtolower(trim(($cat)),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

    $node["raw_categories"] = $cat;
    
          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }

    return $node;
  }

  public function ExternalData(array &$node, array $scraped){


  }

}
