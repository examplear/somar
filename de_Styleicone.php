<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class de_Styleicone extends BaseScraper {

  const Company_Id = 8811534;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_Styleicone");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }


    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DE";
    $node["product_id"] = 'styleicone-de-'.(string)$offer->product_id.'-'.substr(md5((string)$offer->deeplink),0,5);
    $node["url"] = utf8_decode((string)$offer->deeplink);
    $node["title"] = utf8_decode(trim((string)$offer->product_name));
    $node["raw_brand"] = utf8_decode((string)$offer->brand);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->normal_price,",");
    $node["image_small_url"] = (string)$offer->image_url;
    $node["raw_sizes"] = explode(",",(string)$offer->size);
    $node["raw_colors"] = [utf8_decode((string)$offer->Colour)];

   $des =  (string) $offer->description;

   if(!empty($des)){   
     $node["description"] =  utf8_decode($des);
   }
   else
   {
    $node["description"]  =  utf8_decode((string) $offer->product_name);
   }

    if ((string)$offer->gender =="Women") {
      $node["gender"] = 1;

    } else if ((string)$offer->gender =="Men") {
      $node["gender"] = 0;

    }else if ((string)$offer->gender =="Unisex") {
      $node["gender"] = 2;
    }
    else {
      $node["gender"] = NULL;
    }

  if(isset($offer->merchant_category) && !empty($offer->merchant_category)){
      $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=de&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->merchant_category)),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

     $node["raw_categories"] = utf8_decode((string)$offer->merchant_category);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
