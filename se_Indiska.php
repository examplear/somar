<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;

class se_Indiska extends BaseScraper {

  //const MQ_URL = "/vagrant/www/mq_demo_2.xml";
  const Company_Id = 7357518;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Indiska");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }






/*
    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id;
    $node["product_id"] = 'indiska-'.(string)$offer->SKU;
    $node["url"] = (string)$offer->TrackingUrl;
    $node["title"] = utf8_decode((string)$offer->Name);
    $node["old_price"] = SH::extractPrice((string)$offer->Price,",");



    if (!filter_var((string)$offer->ImageUrl, FILTER_VALIDATE_URL) === false) {
      $node["image_small_url"] = (string)$offer->ImageUrl;
    }else{
      $node["image_small_url"] = '';
    }
    $node["description"] = utf8_decode((string)$offer->Description);
    $node["summary"] = SH::text_summary(utf8_decode(((string)$offer->Description))); // TODO
    $node["raw_brand"] = (string)$offer->Brand;
    $node["raw_categories"] = utf8_decode( (string) $offer->Category);


    $node["scrape"] = ["html" => ["url" => (string)$offer->ProductUrl, "method" => "GET"]];
    return $node;
  }

  public function ExternalData(array &$node, array $scraped){
    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"].":".$node["scrape"]["html"]["url"]." ";
      return;
    }
    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);
    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;

     $price = HH::querySelector($crawler, ".product-price-container .product-price");
      if($price === NULL){
        $price = "";
      }else {
        $price = $price->nodeValue;
      }

    $price = preg_replace('/\s+/', ' ', trim(strip_tags($price)));
    $price = explode("SEK", $price)[0];


    $new_value = SH::extractPrice($price,","," ");
    $node["new_price"] = $new_value;


      unset($crawler);
    }

    return $node;
  }


}

*/


    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'indiska-se-'.md5((string)$offer->SKU.(string)$offer->TrackingUrl);
    $node["url"] = utf8_decode((string)$offer->TrackingUrl);
    $node["title"] = utf8_decode(trim((string)$offer->Name));
    $node["raw_brand"] = utf8_decode((string)$offer->Brand);
    $node["new_price"] = SH::extractPrice((string)$offer->Price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->OriginalPrice,",");
    $node["image_small_url"] = (string)$offer->ImageUrl;
  //  $node["description"] = utf8_decode((string)$offer->Description);

    $des =  (string) $offer->Description;

    if(!empty($des)){   
     $node["description"] =  utf8_decode($des);
    }
    else
    {
     $node["description"]  =  utf8_decode((string) $offer->Name);
    }

    if(isset($offer->Category) && !empty($offer->Category)){
        $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->Category)),'UTF-8'));
        $c = json_decode(file_get_contents($catpath));

        $node["raw_categories"] = utf8_decode((string)$offer->Category);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
         $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

   public function ExternalData(array &$node, array $html){

  }


}
