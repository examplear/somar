<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class de_Canda extends BaseScraper {

  const Company_Id = 9211218;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_Canda");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DE";
    $node["product_id"] = 'canda-de-'.(string)$offer->Details->ProductID.'-'.substr(md5((string)$offer->Deeplinks->Product),0,5);
    $node["url"] = utf8_decode((string)$offer->Deeplinks->Product);
    $node["title"] = htmlspecialchars_decode(trim((string)$offer->Properties->Property[10]->attributes()->Text));
    $node["raw_brand"] = htmlspecialchars_decode((string)$offer->Details->Brand);
    $node["new_price"] = SH::extractPrice((string)$offer->Price->Price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->Price->PriceOld,",");
    $node["image_small_url"] = (string)$offer->Images->Img[1]->URL;
    $node["description"] = htmlspecialchars_decode((string)$offer->Details->Description);
    $node["raw_sizes"]  = explode("/",(string)$offer->Properties->Property[9]->attributes()->Text); 
    $node["raw_colors"] = explode("/",(string)$offer->Properties->Property[5]->attributes()->Text);  

   if ((string)$offer->Properties->Property[3]->attributes()->Text =="female") {
      $node["gender"] = 1;

    } else if ((string)$offer->Properties->Property[3]->attributes()->Text =="male") {
      $node["gender"] = 0;

    }else if ((string)$offer->Properties->Property[3]->attributes()->Text =="unisex") {
      $node["gender"] = 2;
    }
    else {
      $node["gender"] = NULL;
    }


    $cat =  htmlspecialchars_decode((string)$offer->CategoryPath->ProductCategoryPath);
    $node["raw_categories"] = str_replace('  ', ' ', $cat);

  if(isset($offer->CategoryPath->ProductCategoryPath) && !empty($offer->CategoryPath->ProductCategoryPath)){
  
      $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=de&name_hash='.md5(mb_strtolower(trim($node["raw_categories"]),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
