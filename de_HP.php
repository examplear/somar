<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;

class de_HP extends BaseScraper {

  const Company_Id = 409714;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_HP");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = 409714; //Constant from Drupal;
    $node["product_id"] = 'hp-de-'.(string)$offer->TDProductId.'-'.substr(md5((string)$offer->productUrl),0,5);
    $node["country"] = "DE";
    $node["url"] = (string)$offer->productUrl;
    $node["title"] = ((string)$offer->name);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->previousPrice,",");
    $node["image_small_url"] = (string)$offer->imageUrl;
    $node["description"] = ((string)$offer->description);
    $node["summary"] = SH::text_summary((string)$offer->description);
    $node["raw_brand"] = utf8_decode((string)$offer->brand);
    $node["raw_categories"] = (trim((string)$offer->merchantCategoryName));

    //$node["raw_sizes"] = [(string)$offer->size];
    //$node["raw_colors"] = [(string)$offer->fields->product_color];


  return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
