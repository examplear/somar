<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class se_Halens extends BaseScraper {

  const Company_Id = 5282640;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Halens");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'halens-'.(string)$offer->SKU;
    $node["url"] = (string)$offer->TrackingUrl;
    $node["title"] = utf8_decode(trim((string)$offer->Name));
    $node["raw_brand"] = utf8_decode((string)$offer->Brand);
    $node["new_price"] = (string)number_format((int) $offer->Price,2,'.');
    $node["old_price"] = (string)number_format((int)$offer->OriginalPrice,2,'.');
    //$node["raw_colors"] = (string)$offer->color;
    $node["image_small_url"] = (string)$offer->ImageUrl;
    $node["description"] = utf8_decode((string)$offer->Description);

    $nam =  (array) $offer->Extras->Name;
    $val =  (array) $offer->Extras->Value;

    if($nam[0] == 'GENDER'){
      $gen = ' - '.$val[0];
    }
    else if($nam[1] == 'GENDER'){
      $gen = ' - '.$val[1];
      }
    else{
      $gen = '';
    }

    $node["raw_categories"] = utf8_decode((string)$offer->Category.$gen);

    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }


}
