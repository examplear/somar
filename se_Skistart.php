<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
header('Content-Type: text/html; charset=utf-8');
class se_Skistart extends BaseScraper {

  const Company_Id = 9636189;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Skistart");
  }
  public function GetRandomIndex(): int {
      return rand(70000,10000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'skistart-se-'.md5((string)$offer->SKU.(string)$offer->TrackingUrl);
    $node["url"] = utf8_decode((string)$offer->TrackingUrl);
    $node["title"] = utf8_decode(trim((string)$offer->Name));    
    $node["raw_brand"] = str_ireplace('Bj¿rn D¿hlie','Bjørn Dæhlie',utf8_decode((string)$offer->Brand));
    $node["new_price"] = SH::extractPrice((string)$offer->Price,",");
    $node["image_small_url"] = (string)$offer->ImageUrl;
    $node["description"] = html_entity_decode(utf8_decode((string)$offer->Name));

    $cat =  utf8_decode((string)$offer->Category);
    $node["raw_categories"] = str_replace('  ', ' ', $cat);

    if(isset($offer->Category) && !empty($offer->Category)){
     $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim($node["raw_categories"]),'UTF-8'));

        $c = json_decode(file_get_contents($catpath));

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
         $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }
    }

    $node["scrape"] = ["html" => ["url" => (string) $offer->ProductUrl, "method" => "GET"]];

    return $node;
  }

   public function ExternalData(array &$node, array $scraped){

    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"].":".$node["scrape"]["html"]["url"]." ";
      return;
    }
    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);
    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;
  
      $oldprice = SH::extractPrice(HH::qVal($crawler, ".product-options-bottom .price-box .old-price .price"), ",", ".");

      if($oldprice == "")
      {
         $node["old_price"] = SH::extractPrice(HH::qVal($crawler, ".add-to-box .price-box .old-price .price"), ",", ".");
      }
      else
      {
         $node["old_price"] = $oldprice;
      }

     // $node["old_price"] = SH::extractPrice(HH::qVal($crawler, ".product-options-bottom .price-box .old-price .price-including-tax"), ",", ".");
      
      unset($crawler);
    }

    return $node;

  }
}

 