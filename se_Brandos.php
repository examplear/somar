<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;

class se_Brandos extends BaseScraper {

  const Company_Id = 21;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Brandos");
  }
  public function GetRandomIndex(): int {
      return rand(10000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'brandos-se-'.(string)$offer->TDProductId.'-'.substr(md5((string)$offer->productUrl),0,10);
    $node["url"] = (string)$offer->productUrl;
    $node["title"] = (string)$offer->name;
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    //$node["old_price"] = SH::extractPrice((string)$offer->previousPrice,",");
    $node["image_small_url"] = (string)$offer->fields->imageLargeURL;
    $node["description"] = (string)$offer->description;
    $node["summary"] = SH::text_summary((string)$offer->description);
    $node["raw_brand"] = (string)$offer->brand;
   // $node["raw_sizes"] = [(string)$offer->fields->fitting];
    $node["raw_colors"] = [utf8_decode((string)$offer->fields->color)];


    if(isset($offer->merchantCategoryName) && !empty($offer->merchantCategoryName)){
        $catpath = 'http://beta.smartster.no/driver.php?table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim((string)$offer->merchantCategoryName),'UTF-8'));

        $c = json_decode(file_get_contents($catpath));

        $node["raw_categories"] = (string)$offer->merchantCategoryName;

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
         $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }


    //$node["scrape"] = ["html" => ["url" => (string) $offer->ProductUrl, "method" => "GET"]];


    return $node;
  }

  public function ExternalData(array &$node, array $scraped){



  }
}
