<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class de_Furniturebox extends BaseScraper {

  const Company_Id = 1629850;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_Furniturebox");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DE";
    $node["product_id"] = 'furniturebox-de-'.(string)$offer->ID.'-'.substr(md5((string)$offer->URL),0,5);
    $node["url"] = utf8_decode((string)$offer->URL);
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->properties->brand->value);
    $node["old_price"] = SH::extractPrice((string)$offer->price->amount,",");
    $node["image_small_url"] = (string)$offer->properties->image_link->value;
   // $node["description"] = utf8_decode((string)$offer->description);
    
   $des = utf8_decode((string)$offer->description);
   if(!empty($des)){
      $node["description"] = $des;
   }
   else
   {
      $node["description"]  =  utf8_decode(trim((string)$offer->name));
   }

    $url =  utf8_decode((string)$offer->URL);
    $node["scrape"] = ["html" => ["url" =>  urldecode(substr($url,strpos($url,'u=http')+2)), "method" => "GET"]];

  if(isset($offer->properties->product_type->value) && !empty($offer->properties->product_type->value)){
      $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=de&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->properties->product_type->value)),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));
  
     $node["raw_categories"] = utf8_decode((string)$offer->properties->product_type->value);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

   public function ExternalData(array &$node, array $scraped){

    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"].":".$node["scrape"]["html"]["url"]." ";
      return;
    }
    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);
    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;

      $node["new_price"] = SH::extractPrice(HH::qVal($crawler, ".PrisREA"), "", ".");


      unset($crawler);
    }

    return $node;

  }
}
