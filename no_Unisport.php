<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class no_Unisport extends BaseScraper {

  const Company_Id = 1144464;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "no");
    $this->reader = new XML2Reader(parent::GetUrl(),"no_Unisport");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "NOK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "NO";
    $node["product_id"] = 'unisport-no-'.(string)$offer->ID.'-'.substr(md5((string)$offer->URL),0,5);
    $node["url"] = utf8_decode((string)$offer->URL);
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->properties->brand->value);
    $node["new_price"] = SH::extractPrice((string)$offer->price->amount,",");
    $node["old_price"] = SH::extractPrice((string)$offer->properties->fromPrice->value,",");
    $node["image_small_url"] = (string)$offer->images->image;
   // $node["description"] = utf8_decode((string)$offer->description);
    $node["raw_sizes"] = [utf8_decode((string)$offer->size)];
  

   $des = utf8_decode((string)$offer->description);
   if(!empty($des)){
      $node["description"] = $des;
   }
   else
   {
      $node["description"]  =  utf8_decode(trim((string)$offer->name));
   }


   if ((string)$offer->properties->gender->value =="female") {
      $node["gender"] = 1;

    } else if ((string)$offer->properties->gender->value =="male") {
      $node["gender"] = 0;

    }else if ((string)$offer->properties->gender->value =="Unisex") {
      $node["gender"] = 2;
    }
    else {
      $node["gender"] = NULL;
    }

  if(isset($offer->properties->categoryPath->value) && !empty($offer->properties->categoryPath->value)){

  $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=no&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->properties->categoryPath->value)),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

     $node["raw_categories"] = utf8_decode((string)$offer->properties->categoryPath->value);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
