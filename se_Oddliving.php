<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;

class se_Oddliving extends BaseScraper {

  const Company_Id = 9005527;
  protected JSONReader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new JSONReader(parent::GetUrl(),"se_Oddliving");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }
    $offer = (array) $offer;
    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'oddliving-se-'.(string)$offer['_id'].'-'.md5((string)$offer['link']);
    $node["url"] = (string)$offer['link'];
    $node["title"] = trim((string)$offer['title']);
    $node["description"] = (string)$offer['description'];
    $node["image_small_url"] = (string)$offer['g-image_link'];
    $node["new_price"] = SH::extractPrice((string)$offer['g-sale_price'],",");
    $node["old_price"] = SH::extractPrice((string)$offer['g-price'],",");
    $node["raw_brand"] = utf8_decode((string)$offer['g-brand']);

    if(isset($offer['Kategori']) && !empty($offer['Kategori'])){
        $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(htmlspecialchars_decode((string)$offer['Kategori'])),'UTF-8'));

        $c = json_decode(file_get_contents($catpath));

        $node["raw_categories"] = htmlspecialchars_decode((string)$offer['Kategori']);

        if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
            $ids =  explode(',', $c[0]->tax_group);
            foreach($ids as $id){
              $node["categories"][] = (int) $id;
            }
        }
    }

    return $node;
  }

  public function ExternalData(array &$node, array $html){
  }
}
