<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class no_Eleven extends BaseScraper {

  const Company_Id = 4394047;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "no");
    $this->reader = new XML2Reader(parent::GetUrl(),"no_Eleven");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "NOK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'eleven-no-'.(string)$offer['zupid'];
    $node["country"] = "NO";
    $node["url"] = (string)$offer->deepLink;
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->manufacturer);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->oldPrice,",");
    //$node["raw_colors"] = (string)$offer->color;
   // $node["raw_sizes"] = [(string)$offer->size];
    $node["image_small_url"] = (string)$offer->largeImage;
    $node["description"] = utf8_decode((string)$offer->longDescription);
    $node["summary"] = SH::text_summary(utf8_decode((string)$offer->description));
    $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath);

    $start = strpos((string)$offer->deepLink, '[[');
    $end = strpos((string)$offer->deepLink, '.html?');

    $cu ='http://eleven.no/'.substr((string)$offer->deepLink,$start+2,$end-$start+3);
    $node["scrape"] = ["html" => ["url" => $cu, "method" => "GET"]];


    return $node;
  }

  public function ExternalData(array &$node, array $scraped){

    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"].":".$node["scrape"]["html"]["url"]." ";
      return;
    }
    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);
    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;

      $node["old_price"] = SH::extractPrice(HH::qVal($crawler, "#d-primary .pr-price-reg"), "", ".");


      unset($crawler);
    }

    return $node;

  }
}
