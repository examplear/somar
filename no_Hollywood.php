<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class no_Hollywood extends BaseScraper {

  const Company_Id = 5515697;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "no");
    $this->reader = new XML2Reader(parent::GetUrl(),"no_Hollywood");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "NOK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "NO";
    $node["product_id"] = 'hollywood-no-'.md5((string)$offer->SKU.(string)$offer->TrackingUrl);
    $node["url"] = utf8_decode((string)$offer->TrackingUrl);
    $node["title"] = utf8_decode(trim((string)$offer->Name));
    $node["raw_brand"] = utf8_decode((string)$offer->Brand);
    $node["new_price"] = SH::extractPrice((string)$offer->Price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->OriginalPrice,",");
    $node["image_small_url"] = (string)$offer->ImageUrl;
   // $node["description"] = utf8_decode((string)$offer->Description);

   $des = utf8_decode((string)$offer->Description);
   if(!empty($des)){
      $node["description"] = $des;
   }
   else
   {
      $node["description"]  =  utf8_decode(trim((string)$offer->Name));
   }


    $nam =   $offer->Extras->Name;
    $val =   $offer->Extras->Value;

    if($nam[0] == 'COLOUR'){
        $col1 = $val[0];
    }
    else if($nam[1] == 'COLOUR'){
        $col1 = $val[1];
    }
    else if($nam[2] == 'COLOUR'){
        $col1 = $val[2];
    }
    else{
      $col1 = '';
    }

    $node["raw_colors"] = [utf8_decode((string)$col1)];

    $nam2 =   $offer->Extras->Name;
    $val2 =   $offer->Extras->Value;

    if($nam2[0] == 'SIZE'){
        $size2 = $val2[0];
    }
    else if($nam2[1] == 'SIZE'){
        $size2 = $val2[1];
    }
    else if($nam2[2] == 'SIZE'){
        $size2 = $val2[2];
    }
    else{
      $size2 = '';
    }

    $node["raw_sizes"] = explode(",",(string)$size2);


    $nam3 =   $offer->Extras->Name;
    $val3 =   $offer->Extras->Value;

    if($nam3[0] == 'GENDER'){
        $gen = $val3[0];
    }
    else if($nam3[1] == 'GENDER'){
        $gen = $val3[1];
    }
    else if($nam3[2] == 'GENDER'){
        $gen = $val3[2];
    }
    else{
      $gen = '';
    }

    if($gen == "Dame"){
      $node["gender"] = 1;
      $node["raw_categories"] = utf8_decode((string)$offer->Category.' - Dame');

    } elseif($gen == "Herre"){
      $node["gender"] = 0;
      $node["raw_categories"] = utf8_decode((string)$offer->Category.' - Herre');
    }
    elseif($gen == "Unisex"){
      $node["gender"] = 2;
      $node["raw_categories"] = utf8_decode((string)$offer->Category.' - Unisex');
    }
    else {
      $node["gender"] = NULL;
      $node["raw_categories"] = utf8_decode((string)$offer->Category);
    }

  if(isset($offer->Category) && !empty($offer->Category)){

  $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=no&name_hash='.md5(mb_strtolower(trim($node["raw_categories"]),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

   //  $node["raw_categories"] = utf8_decode((string)$offer->Category);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }
    }
    return $node;
  }

   public function ExternalData(array &$node, array $html){

  }

}