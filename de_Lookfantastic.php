<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class de_Lookfantastic extends BaseScraper {

  const Company_Id = 7366297;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_Lookfantastic");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'lookfantastic-de-'.(string)$offer['zupid'].'-'.substr(md5((string)$offer->deepLink),0,5);
    $node["country"] = "DE";
    $node["url"] = (string)$offer->deepLink;
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->manufacturer);

     if ((string)$offer->gender =="Damen") {
      $node["gender"] = 1;
    } else if ((string)$offer->gender=="Herren") {
      $node["gender"] = 0;
    }
    else {
      $node["gender"] = NULL;
    }

    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->oldPrice,",");
    $node["raw_colors"] = [(string)$offer->color];

    $node["raw_sizes"] = [(string)$offer->size];
    $node["image_small_url"] = (string)$offer->largeImage;
    $node["description"] = utf8_decode((string)$offer->longDescription);
    $node["summary"] = SH::text_summary(utf8_decode((string)$offer->description));
    $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath);

    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }
}
