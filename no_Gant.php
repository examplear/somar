<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class no_Gant extends BaseScraper {

  const Company_Id = 5608078;
  protected JSONReader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "no");
    $this->reader = new JSONReader(parent::GetUrl(),"no_Gant");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "NOK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $offer = (array) $offer;
    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "NO";
    $node["product_id"] = 'gant-no-'.(string)$offer['_id'].'-'.md5((string)$offer['link']);
    $node["url"] = (string)$offer['link'];
    $node["title"] = htmlspecialchars_decode(trim((string)$offer['title']));
    $node["description"] = htmlspecialchars_decode((string)$offer['description']);
    $node["image_small_url"] = (string)$offer['g-image_link'];
    $node["new_price"] = SH::extractPrice((string)$offer['g-sale_price'],",");
    $node["old_price"] = SH::extractPrice((string)$offer['g-price'],",");
    $node["raw_brand"] = htmlspecialchars_decode((string)$offer['g-brand']);
    $node["raw_sizes"] = [utf8_decode((string)$offer['g-size'])];
    $node["raw_colors"] = [utf8_decode((string)$offer['g-color'])];
  

    if ((string)$offer['g-gender'] =="female") {
      $node["gender"] = 1;
      $node["raw_categories"] = htmlspecialchars_decode((string)$offer['g-google_product_category'].' - female');

    } else if ((string)$offer['g-gender'] =="male") {
      $node["gender"] = 0;
      $node["raw_categories"] = htmlspecialchars_decode((string)$offer['g-google_product_category'].' - male');

    }else if ((string)$offer['g-gender'] =="unisex") {
      $node["gender"] = 2;
      $node["raw_categories"] = htmlspecialchars_decode((string)$offer['g-google_product_category'].' - unisex');
    }
    else {
      $node["gender"] = NULL;
      $node["raw_categories"] = htmlspecialchars_decode((string)$offer['g-google_product_category']);
    }

    if(isset($offer['g-google_product_category']) && !empty($offer['g-google_product_category'])){
       
        $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=no&name_hash='.md5(mb_strtolower(trim( $node["raw_categories"]),'UTF-8'));

        $c = json_decode(file_get_contents($catpath));

      //  $node["raw_categories"] = utf8_decode((string)$offer['g-google_product_category']);

        if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
            $ids =  explode(',', $c[0]->tax_group);
            foreach($ids as $id){
              $node["categories"][] = (int) $id;
            }
        }
    }

    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }
}
