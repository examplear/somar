<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class de_JBL extends BaseScraper {

  const Company_Id = 7755491;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "de");
    $this->reader = new XML2Reader(parent::GetUrl(),"de_JBL");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "EUR";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DE";
    $node["product_id"] = 'jbl-de-'.(string)$offer->ID;
    $node["url"] = (string)$offer->URL;
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->properties->product_brand->value);
    $node["image_small_url"] = (string)$offer->properties->product_image1->value;
    $node["new_price"] = SH::extractPrice((string)$offer->properties->product_current_price->value,",");
    $node["old_price"] = SH::extractPrice((string)$offer->properties->product_list_price->value,",");
    $node["raw_colors"] = [utf8_decode((string)$offer->properties->product_color->value)];
    $node["description"] = utf8_decode((string)$offer->properties->product_long_description->value);
    $node["summary"] = utf8_decode(SH::text_summary((string)$offer->name));
    $node["raw_categories"] = utf8_decode((string)$offer->properties->product_breadcrumb4->value);
    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
