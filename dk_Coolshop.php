<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class dk_Coolshop extends BaseScraper {

  const Company_Id = 7976159;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "dk");
    $this->reader = new XML2Reader(parent::GetUrl(),"dk_Coolshop");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "DKK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DK";
    $node["product_id"] = 'coolshop-dk-'.(string)$offer->SKU;
    $node["url"] = 'https://salestring.go2cloud.org/aff_c?offer_id=29&aff_id=1705&url='.(string)$offer->url.'?utm_medium=affiliate&utm_source=salestring&utm_campaign={affiliate_name}';
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->manufacturer);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->guide_price,",");
    $node["image_small_url"] = (string)$offer->image;
    $node["raw_colors"] = (string)$offer->Colour;
    $node["raw_sizes"] = (string)$offer->size;
    $node["description"] = utf8_decode((string)$offer->name);
    $node["summary"] = utf8_decode(SH::text_summary((string)$offer->name));

    if($offer->gender == "female"){
      $node["gender"] = 1;
    } elseif($offer->gender == "male"){
      $node["gender"] = 0;
    }
    else {
      $node["gender"] = 2;
    }

    if ((string)$offer->gender =="male") {
      $node["raw_categories"] = utf8_decode(trim((string)$offer->category.' - '.$offer->subcategory.' - male'));

    } else if ((string)$offer->gender=="female") {
       $node["raw_categories"] = utf8_decode(trim((string)$offer->category.' - '.$offer->subcategory.' - female'));
    } else {
      $node["raw_categories"] = utf8_decode(trim((string)$offer->category.' - '.$offer->subcategory));
    }

  if(isset($offer->category) && !empty($offer->category)){
      $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=dk&name_hash='.md5(mb_strtolower(trim($node["raw_categories"]),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

    // $node["raw_categories"] = utf8_decode((string)$offer->merchantCategory);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }
    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }

}
