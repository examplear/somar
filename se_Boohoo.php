<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
header('Content-Type: text/html; charset=utf-8');
class se_Boohoo extends BaseScraper {

  const Company_Id = 6157867;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Boohoo");
  }
  public function GetRandomIndex(): int {
      return rand(30000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'boohoo-se-'.(string)$offer['zupid'].'-'.substr(md5((string)$offer->deepLink),0,5);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->oldPrice,",");
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["raw_brand"] = utf8_decode((string)$offer->manufacturer);
    $node["image_small_url"] = (string)$offer->smallImage;
    $node["image_large_url"] = (string)$offer->largeImage;
    $node["url"] = (string)$offer->deepLink;
    $node["raw_sizes"] = [(string)$offer->size];
    $node["raw_colors"] = [utf8_decode((string)$offer->color)];
    $node["description"] = utf8_decode((string)$offer->description);
    $node["summary"] = utf8_decode(SH::text_summary((string)$offer->longDescription));

    if ((string)$offer->gender =="Female") {
      $node["gender"] = 1;

    } else if ((string)$offer->gender =="male") {
      $node["gender"] = 0;

    }
    else if ((string)$offer->gender =="Unisex") {
      $node["gender"] = 2;
    }
    else {
      $node["gender"] = NULL;
    }

    if(isset($offer->merchantCategoryPath) && !empty($offer->merchantCategoryPath)){
        $catpath = 'http://beta.smartster.no/driver.php?table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->merchantCategoryPath)),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

     $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryPath);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }

    return $node;
  }

   public function ExternalData(array &$node, array $html){

  }


}
