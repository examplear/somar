<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class se_Fitnesssport extends BaseScraper {

  const Company_Id = 7308019;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Fitnesssport");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'fitnesssport-'.(string)$offer['ID'];
    $node["url"] = (string)$offer->URL;
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["image_small_url"] = (string)$offer->images->image;
    $node["description"] = utf8_decode((string)$offer->properties->property[1]->value);
    $node["summary"] = utf8_decode(trim((string)$offer->name));
    $node["new_price"] = SH::extractPrice((string)$offer->properties->property[3]->value,",");
    $node["old_price"] = SH::extractPrice((string)$offer->price,",");
    $node["raw_categories"] = utf8_decode((string)$offer->categories->category);
    $node["raw_brand"] = utf8_decode((string)$offer->properties->property[10]->value);
    $node["raw_colors"] = (string)$offer->properties->property[7]->value;

    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }


}
