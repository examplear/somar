<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class se_Bodystore extends BaseScraper {

  const Company_Id = 10425293;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Bodystore");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'bodystore-se-'.(string)$offer->TDProductId.'-'.substr(md5((string)$offer->productUrl),0,5);
    $node["url"] = (string)$offer->productUrl;
    $node["old_price"] = SH::extractPrice((string)$offer->price,",");
    $node["title"] = htmlspecialchars_decode(trim((string)$offer->name));
    $node["image_small_url"] = (string)$offer->imageUrl;
    $node["description"] = htmlspecialchars_decode((string)$offer->description);
    $node["raw_brand"] = htmlspecialchars_decode((string)$offer->brand);

    if(isset($offer->merchantCategoryName) && !empty($offer->merchantCategoryName)){
    
     $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(htmlspecialchars_decode((string)$offer->merchantCategoryName)),'UTF-8'));

        $c = json_decode(file_get_contents($catpath));

       $node["raw_categories"] = htmlspecialchars_decode((string)$offer->merchantCategoryName);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
         $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }

    $url =  utf8_decode((string)$offer->productUrl);

    $input = urldecode(substr($url,strpos($url,'url(http')+3));

    $strurl = urldecode(str_replace(array("(", ")"), '', $input));

    $node["scrape"] = ["html" => ["url" => $strurl, "method" => "GET"]];

    return $node;
  }


  public function ExternalData(array &$node, array $scraped){

    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"].":".$node["scrape"]["html"]["url"]." ";
      return;
    }
    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);
    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;

      $node["new_price"] = SH::extractPrice(HH::qVal($crawler, ".price-wrapper .price-box .special-price .price"), ",", ".");

      unset($crawler);
    }

    return $node;

  }
}
