<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class dk_Stayhard extends BaseScraper {

  const Company_Id = 8050097;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "dk");
    $this->reader = new XML2Reader(parent::GetUrl(),"dk_Stayhard");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "DKK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DK";
    $node["product_id"] = 'stayhard-dk-'.(string)$offer->SKU.'-'.md5((string)$offer->ProductUrl);
    $node["url"] = (string)$offer->TrackingUrl;
    $node["title"] = utf8_decode(trim((string)$offer->Name));
    $node["raw_brand"] = utf8_decode((string)$offer->Brand);
    $node["new_price"] = SH::extractPrice((string)$offer->Price,",");
    $node["image_small_url"] = (string)$offer->ImageUrl;
    $node["description"] = utf8_decode((string)$offer->Name);
    $node["raw_categories"] = utf8_decode((string)$offer->Category);    
    $node["scrape"] = ["html" => ["url" => (string)$offer->ProductUrl, "method" => "GET"]];

    return $node;
  }

  public function ExternalData(array &$node, array $scraped){

  if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"].":".$node["scrape"]["html"]["url"]." ";
      return;
    }
    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);
    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;
     $node["old_price"] = SH::extractPrice(HH::qVal($crawler, ".priceContainer  .regularPrice"), "", ".");
      unset($crawler);
    }

    return $node;

  }


}
