<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  //header('Content-Type: text/html; charset=utf-8');
class dk_Manillo extends BaseScraper {

  const Company_Id = 9588293;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "dk");
    $this->reader = new XML2Reader(parent::GetUrl(),"dk_Manillo");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "DKK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DK";
    $node["product_id"] = 'manillo-dk-'.utf8_decode((string)$offer->ProductSKU).'-'.substr(md5((string)$offer->ProductURL),0,5);
    $node["url"] = utf8_decode((string)$offer->ProductURL);
    $node["title"] = utf8_decode(trim((string)$offer->ProductName));
    $node["raw_brand"] = utf8_decode((string)$offer->ManufacturerSKU);
    $node["new_price"] = SH::extractPrice((string)$offer->Price,",");
    $node["image_small_url"] = (string)$offer->imageURL;
    $node["description"] = utf8_decode((string)$offer->Description);

    if(isset($offer->Category) && !empty($offer->Category)){
     $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=dk&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->Category)),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

     $node["raw_categories"] = utf8_decode((string)$offer->Category);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }

   $url =  utf8_decode((string)$offer->ProductURL);
   $node["scrape"] = ["html" => ["url" =>  urldecode(substr($url,strpos($url,'url=http')+4)), "method" => "GET"]];
 
    return $node;
  }

 public function ExternalData(array &$node, array $scraped){

  if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"].":".$node["scrape"]["html"]["url"]." ";
      return;
    }
    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);
    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;
     $node["old_price"] = SH::extractPrice(HH::qVal($crawler, ".price-box .old-price .price"), ",", ".");
      unset($crawler);
    }

    return $node;

  }


}
