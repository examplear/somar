<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;

class se_Banggood extends BaseScraper {

  const Company_Id = 8974478;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Banggood");
  }
  public function GetRandomIndex(): int {
      return rand(70000,10000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'banggood-se-'.(string)$offer->ID.'-'.substr(md5((string)$offer->URL),0,5);
    $node["url"] = (string)$offer->URL;
    //$node["new_price"] = "";
    //$node["old_price"] = SH::extractPrice((string)$offer->properties->fromPrice->value,",");
    $node["title"] = utf8_decode(trim((string)$offer->name));
    $node["image_small_url"] = (string)$offer->images->image;
    $node["description"] = utf8_decode((string)$offer->description);
    $node["summary"] = utf8_decode((string)$offer->description); // TODO
    $node["raw_brand"] = utf8_decode((string)$offer->properties->brand->value);
    //$node["raw_categories"] = utf8_decode((string)$offer->properties->categoryPath->value);



    $string = 'Intimate Apparel';
    $pos = strpos($offer->properties->categoryPath->value, $string);
    if ($pos === false) {
      $node["new_price"] = SH::extractPrice((string)$offer->price->amount,",");
      $node["old_price"] = SH::extractPrice((string)$offer->properties->fromPrice->value,",");
    }
//$categories = array( "Intimate Apparel > Adult Games","Intimate Apparel > Sexy Lingerie > Baby dolls","Intimate Apparel > Stockings & Garters > Stockings","Intimate Apparel > Cosplay & Costumes","Intimate Apparel > Adult Games > Clamps","Intimate Apparel > Adult Games > Cuffs & collars","Intimate Apparel > Adult Games > Flogging toys","Intimate Apparel > Adult Games > Gags & masks","Intimate Apparel > Adult Games > Others","Intimate Apparel > Adult Games > SM wear","Intimate Apparel > Adult Games > Tricky toys","Intimate Apparel > Adult Toys > Anal toys","Intimate Apparel > Adult Toys > Masturbators","Intimate Apparel > Adult Toys > Penis rings","Intimate Apparel > Adult Toys > Teasers & Pumps","Intimate Apparel > Adult Toys > Vibrator");
 /*if(!in_array((parent::Combine($offer->properties->categoryPath->value)),$categories)){

               $node["new_price"] = SH::extractPrice((string)$offer->price->amount,",");
               $node["old_price"] = SH::extractPrice((string)$offer->properties->fromPrice->value,",");
 }*/


    if(isset($offer->properties->categoryPath->value) && !empty($offer->properties->categoryPath->value)){
        $catpath = 'http://beta.smartster.no/driver.php?table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->properties->categoryPath->value)),'UTF-8'));

        $c = json_decode(file_get_contents($catpath));

        $node["raw_categories"] = utf8_decode((string)$offer->properties->categoryPath->value);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
         $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }


    return $node;
  }

   public function ExternalData(array &$node, array $html){

  }


}
