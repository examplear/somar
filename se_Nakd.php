<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
class se_Nakd extends BaseScraper {

  const Company_Id = 9239957;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_Nakd");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = 'nakd-se-'.(string)$offer->TDProductId.'-'.md5((string)$offer->productUrl);
    $node["url"] = utf8_decode((string)$offer->productUrl);
    $node["title"] = htmlspecialchars_decode(trim((string)$offer->name));
    $node["raw_brand"] = htmlspecialchars_decode((string)$offer->brand);
    $node["old_price"] = SH::extractPrice((string)$offer->price,",");
    $node["image_small_url"] = (string)$offer->imageUrl;
    $node["description"] = htmlspecialchars_decode((string)$offer->description);
    $node["summary"] = htmlspecialchars_decode(SH::text_summary((string)$offer->description));
    $node["raw_sizes"] = explode(",",(string)$offer->size);
    
    $remove_http = str_replace('http://', '', ((string)$offer->productUrl));
    $split_url = explode('url', $remove_http);
    $input = $split_url[1];
    $strurl = urldecode(str_replace(array("(", ")"), '', $input));

    $node["scrape"] = ["html" => ["url" => $strurl, "method" => "GET"]];

    if(isset($offer->merchantCategoryName) && !empty($offer->merchantCategoryName)){
        $catpath = 'http://beta.smartster.no/driver.php?table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(htmlspecialchars_decode((string)$offer->merchantCategoryName)),'UTF-8'));

        $c = json_decode(file_get_contents($catpath));

        $node["raw_categories"] = htmlspecialchars_decode((string)$offer->merchantCategoryName);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
         $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }
    }
    return $node;
  }

  public function ExternalData(array &$node, array $scraped){

    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"].":".$node["scrape"]["html"]["url"]." ";
      return;
    }
    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);
    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;

     $node["new_price"] = SH::extractPrice(HH::qVal($crawler, ".price"),"", "");
      unset($crawler);
    }
    return $node;
  }
}
