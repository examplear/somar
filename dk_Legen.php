<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;
  header('Content-Type: text/html; charset=utf-8');
class dk_Legen extends BaseScraper {

  const Company_Id = 9514410;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "dk");
    $this->reader = new XML2Reader(parent::GetUrl(),"dk_Legen");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "DKK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DK";
    $node["product_id"] = 'legen-dk-'.utf8_decode((string)$offer->produktid).'-'.substr(md5((string)$offer->vareurl),0,5);
    $node["url"] = utf8_decode((string)$offer->vareurl);
    $node["title"] = utf8_decode((string)$offer->produktnavn);
    //$node["raw_brand"] = utf8_decode((string)$offer->Brand);
    $node["new_price"] = SH::extractPrice((string)$offer->pris,",");
    $node["old_price"] = SH::extractPrice((string)$offer->originalpris,",");
    $node["image_small_url"] = (string)$offer->billedurl;
    $node["description"] = utf8_decode((string)$offer->produktbeskrivelse);
   
    if(isset($offer->kategorinavn) && !empty($offer->kategorinavn)){
     $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=dk&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->kategorinavn)),'UTF-8'));
     
     $c = json_decode(file_get_contents($catpath));

     $node["raw_categories"] = utf8_decode((string)$offer->kategorinavn);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }


    return $node;
  }


public function ExternalData(array &$node, array $html){

  }
}
