<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Monolog\Logger;

class se_Passionhome extends BaseScraper {

  const Company_Id = 9368163;

  protected CSVReader2 $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new CSVReader2(parent::GetUrl(),"se_Passionhome",";");
  }
  public function GetRandomIndex(): int {
      return rand(10000, 100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {

    // loop through the file line-by-line
    if(($data = $this->reader->Next()) === NULL){
      return NULL;
    }

    $offer = $data;
    if($offer && isset($offer[10])){
      $node = [];
      $node["format"] = "full_html";
      $node["company_id"] = self::Company_Id; //Constant from Drupal;
      $node["product_id"] = 'passionhome-'.md5((string)$offer[10]);
      $node["title"] = htmlspecialchars_decode((string)$offer[0]);
      $node["description"] = htmlspecialchars_decode(str_replace('[br]','',(string)$offer[1]));
      $node["new_price"] = SH::extractPrice((string)$offer[2],",");
      $node["old_price"] = SH::extractPrice((string)$offer[3],",");
      $node["image_small_url"] = (string)$offer[4];
      $node["raw_brand"] = utf8_decode((string)$offer[9]);
      $node["url"] = utf8_decode((string)$offer[10]);

     if(isset($offer[6]) && !empty($offer[6])){
        $catpath = 'http://beta.smartster.no/driver.php?t='.time().'&table=ImportStrings&method=search_hash&country=se&name_hash='.md5(mb_strtolower(trim(htmlspecialchars_decode((string)$offer[6])),'UTF-8'));

       $c = json_decode(file_get_contents($catpath));

       $node["raw_categories"] = htmlspecialchars_decode((string)$offer[6]);

            if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
             $ids =  explode(',', $c[0]->tax_group);
             foreach($ids as $id){
               $node["categories"][] = (int) $id;
             }
        }

      }
      return $node;
    }
    return NULL;
  }

  public function ExternalData(array &$node, array $html){

  }


}
