<?hh
use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;

class se_MQ extends BaseScraper {

  //const MQ_URL = "/vagrant/www/mq_demo_2.xml";
  const Company_Id = 1048408;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_mq");
  }
  public function GetRandomIndex(): int {
      return rand(70000,10000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = 1048408; //Constant from Drupal;

    $node["product_id"] = 'mq-'.(string)$offer->TDProductId;
    $node["url"] = (string)$offer->productUrl;
    $node["title"] = (string)$offer->name;
    $node["old_price"] = (string)$offer->price;
    $node["image_small_url"] = (string)$offer->imageUrl;
    $node["description"] = (string)$offer->description;
    $node["summary"] = SH::text_summary((string)$offer->description); // TODO
    $node["raw_brand"] = (string)$offer->brand;


    $node["raw_sizes"] = [(string)$offer->size]; //These are present in feed but empty
    $node["raw_colors"] = [(string)$offer->fields->product_color];


    //Merchantcategoryname contains random data from their DB
    //No unisex
    if(strpos(((string)$offer->merchantCategoryName), "Dam") !== False){
      $node["gender"] = 1;
    } else if(strpos(((string)$offer->merchantCategoryName),"Herr") !== False){
      $node["gender"] = 0;
    }
    else {
      $node["gender"] = NULL;
    }

    //$page = parent::get_web_page((string)$offer->advertiserProductUrl);
    $scrape_url = urldecode(SH::extract("/url\((.+)\)/",(string)$offer->productUrl));


    $node["scrape"] = ["html" => ["url" => $scrape_url, "method" => "GET"]];
    return $node;
  }

  public function ExternalData(array &$node, array $scraped){
    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"].":".$node["scrape"]["html"]["url"]." ";
      return;
    }
    $xpath = HH::XpathFromHtml($scraped["html"]["content"]);
    if($xpath !==  NULL) {
      $result = [];
      $crawler = $xpath;

      $price = HH::querySelector($crawler, "#article #price");
      if($price === NULL){
        $price = "";
      }else {
        $price = $price->nodeValue;
      }

      $new_value = SH::extractPrice($price,"."," ");

      $temp = [];
      $domNodeList = HH::querySelectorAll($crawler, "#breadcrumbs a");
      $cats = parent::Join(_::map($domNodeList, $x ==> $x->nodeValue));

      //$node["new_price"] = $new_value;
      $node["new_price"] = '';
      $node["raw_categories"] = $cats;

      unset($crawler);
    }

    return $node;
  }


}
