<?hh
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;

class dk_Skobox extends BaseScraper {

  const Company_Id = 8430812;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger,self::Company_Id, "dk");
    $this->reader = new XML2Reader(parent::GetUrl(),"dk_Skobox");
  }
  public function GetRandomIndex(): int {
      return rand(70000,100000);
  }
  public function GetCurrency() : string {
    return "DKK";
  }
  public function ScrapeNext() : ?array {
    $offer = $this->reader->Next(true);
    if($offer === NULL){
      return NULL;
    }

    $node = [];
    $node["format"] = "full_html";
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["country"] = "DK";
    $node["product_id"] = 'skobox-dk-'.(string)$offer->TDProductId.'-'.md5((string)$offer->productUrl);
    $node["url"] = (string)$offer->productUrl;
    $node["title"] = (trim((string)$offer->name));
    $node["raw_brand"] = ((string)$offer->brand);
    $node["new_price"] = SH::extractPrice((string)$offer->price,",");
    $node["old_price"] = SH::extractPrice((string)$offer->previousPrice,",");
    $node["image_small_url"] = (string)$offer->imageUrl;
    $node["description"] = ((string)$offer->description);
    $node["summary"] = (SH::text_summary((string)$offer->description));
    $node["raw_colors"] = [(string)$offer->color];
    //$node["raw_sizes"] = [(string)$offer->size];



  if ((string)$offer->fields->Gender == "Dame" || (string)$offer->fields->Gender == "Dreng" ) {
      $node["gender"] = 1;

    } else if ((string)$offer->fields->Gender == "Herre" || (string)$offer->fields->Gender == "Pige") {
      $node["gender"] = 0;

    } else {
      $node["gender"] = NULL;
    }


    if(isset($offer->merchantCategoryName) && !empty($offer->merchantCategoryName)){
        $catpath = 'http://beta.smartster.no/driver.php?table=ImportStrings&method=search_hash&country=dk&name_hash='.md5(mb_strtolower(trim(utf8_decode((string)$offer->merchantCategoryName)),'UTF-8'));

     $c = json_decode(file_get_contents($catpath));

     $node["raw_categories"] = utf8_decode((string)$offer->merchantCategoryName);

          if(isset($c[0]->tax_group) && !empty($c[0]->tax_group)){
           $ids =  explode(',', $c[0]->tax_group);
           foreach($ids as $id){
             $node["categories"][] = (int) $id;
           }
      }

    }


    return $node;
  }

  public function ExternalData(array &$node, array $html){

  }


}
