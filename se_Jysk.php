<?hh

use Symfony\Component\DomCrawler\Crawler;
use \Helpers\StringHelpers\StringHelpers as SH;
use \Helpers\HtmlHelpers\HtmlHelpers as HH;
use Underscore\Underscore as _;
use Globals\Globals as G;
use Monolog\Logger;

class se_Jysk extends BaseScraper {


  const Company_Id = 2544362;
  protected XML2Reader $reader;

  public function __construct(MappingService $cs, OfferService $os, Logger $logger){
    parent::__construct($cs,$os, $logger, self::Company_Id, "se");
    $this->reader = new XML2Reader(parent::GetUrl(),"se_jysk");
  }

  public function GetRandomIndex(): int {
      return rand(60000, 100000);
  }
  public function GetCurrency() : string {
    return "SEK";
  }
  public function ScrapeNext() : ?array {
    do {
      $offer = $this->reader->Next(true);

      if($offer === null){
        return null;
      }
    } while( $offer->changefreq == 'never' || $offer->changefreq == 'monthly' || $offer->changefreq == 'yearly' || strpos((string)$offer->loc,"/taxonomy/term/") > 0);


    $url = (string)$offer->loc;    
    $node = [];
    $node["format"] = "full_html";
    $node["url"] = $url;
    $node["scrape"] = ["html" => ["url" => $url]];
    $node["company_id"] = self::Company_Id; //Constant from Drupal;
    $node["product_id"] = $url; // temporarily set product_id overwrite in ExternalData
    return $node;
  }

  public function ExternalData(array &$node, array $scraped) {

    if($scraped["html"]["status_code"] != 200){
      $node["reason"] .= "Failed to fetch html code:".$scraped["html"]["status_code"]." ";
      return;
    }
    $crawler = HH::XpathFromHtml($scraped["html"]["content"]);


    $product_id = HH::qVal($crawler, ".product-sku");
    if(empty($product_id)) {
      $node["product_id"] = null;
    }
    else{
      $node["product_id"] = 'jysk-'.SH::extract("/([\d]+)/",$product_id);
    }

    $node["old_price"] = SH::extractPrice(HH::qVal($crawler, ".product-sumup span[itemprop=highPrice]"), ",");

    $new_price = SH::extractPrice(HH::qVal($crawler, ".product-sumup span[itemprop=lowPrice]"), ",");

    if($new_price === null){
      $node["new_price"] = "";
    }else {
      $node["new_price"] = SH::extractPrice($new_price, ",");
    }

    $description = HH::querySelector($crawler,"#product-description");
    if($description === null){
      $node["description"] = "";
    }
    else {
      $node["description"] = SH::DomToString($description);
    }
    $node["summary"] = SH::text_summary($node["description"]);

    $url = $node["url"];
    $node["raw_categories"] = substr($url,15,-(strlen($url)-strripos($url,'/')));

    $title = HH::querySelector($crawler,"h1");
    if($title === null){
      $node["title"] = "";
    }
    else {
      $node["title"] = $title->nodeValue;
    }

    $image_small_url = HH::querySelector($crawler,"#product-image-carousel .item a");
    if($image_small_url === null){
      $node["image_small_url"] = "";
    }
    else {
      $node["image_small_url"] = 'http:'.$image_small_url->getAttribute("href");
    }
  }
}
